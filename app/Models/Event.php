<?php

namespace App\Models;

use App\Http\Requests\EventsRequest;
use App\Models\Queries\ApplyQueryOrderBy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
require_once app_path('/helpers.php');

class Event extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;
    
    const UPDATED_AT = 'modified_at';
    const CREATED_AT = 'created_at_events';

    protected $hidden = ['laravel_through_key', 'pivot'];   

    /**
     * Relation NxN with urls
     */
    public function urls()
    {
        return $this->belongsToMany(Url::class, 'events_urls', 'event_id', 'url_id');
    }

    public function previous(){
        return $this->hasOne(Event::class, 'id', 'previous_id');
    }

    public function next(){
        return $this->hasOne(Event::class, 'id', 'next_id');
    }

    public function comics()
    {
        return $this->belongsToMany(Comic::class, 'comics_events', 'event_id', 'comic_id');
    }

    public function characters(){
        return $this->hasManyDeep(Character::class, ['comics_events', Comic::class, 'comics_characters'])->distinct(DB::raw('character_id, event_id'));
    }

    public function creators(){
        return $this->hasManyDeep(Creator::class, ['comics_events', Comic::class, 'comics_creators'])->distinct(DB::raw('creator_id, event_id'));
    }

    public function stories(){
        return $this->hasManyDeep(Story::class, ['comics_events', Comic::class, 'comics_stories'])->distinct(DB::raw('story_id, event_id'));
    }

    public function series(){
        return $this->hasManyDeepFromRelations($this->comics(), (new Comic)->serie())->distinct(DB::raw('serie_id, event_id'));
    }

    /**
     * Apply all relations to return complete data
     */
    public function scopeApplyRelations($scopedQuery)
    {
        return $scopedQuery->with([
            'creators' => function ($query) {
                $query->selectRaw('creators.id, creators.first_name, creators.middle_name, creators.last_name');
                $query->orderBy('creators.id');
                $query->latest()->limit(20);
            },
            'characters' => function ($query) {
                $query->selectRaw('characters.id, characters.name');
                $query->orderBy('characters.id');
                $query->latest()->limit(20);
            },
            'series' => function ($query) {
                $query->selectRaw('series.id, series.title');
                $query->orderBy('series.id');
                $query->latest()->limit(20);
            },
            'comics' => function ($query) {
                $query->selectRaw('comics.id, comics.title');
                $query->orderBy('comics.id');
                $query->latest()->limit(20);
            },
            'stories' => function ($query) {
                $query->selectRaw('stories.id, stories.title');
                $query->orderBy('stories.id');
                $query->latest()->limit(20);
            },            
            'urls'
        ])
            ->withCount([
                'comics',
                'creators' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(creator_id))'));
                },
                'characters' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(character_id))'));
                },
                'series' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(serie_id))'));
                },
                'stories' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(story_id))'));
                },
            ]);
    }

    /**
     * Apply filters from a request
     */
    public function scopeApplyFilters($queryBuilder, EventsRequest $request, $character_id)
    {
        //apply character id filter
        $queryBuilder->whereHas('characters', function ($query) use ($character_id) {
            $query->latest()->where('character_id', $character_id);
        });

        if ($request->get('name')) {
            $queryBuilder->where(DB::raw('lower(title)'), strtolower($request->get('name')));
        }
        if ($request->get('nameStartsWith')) {
            $queryBuilder->whereRaw('lower(title) ilike ?', [strtolower($request->get('nameStartsWith')) . '%']);
        }
        if ($request->get('modifiedSince') && getValidDate($request->get('modifiedSince'))) {
            $queryBuilder->where('modified_at', '>=', getValidDate($request->get('modifiedSince')));
        }

        if ($request->get('creators')) {
            $creatorIds = explode(',', $request->get('creators'));

            $queryBuilder->whereHas('comics.creators', function ($query) use ($creatorIds) {
                $query->latest()->whereIn('creator_id', getValidIds($creatorIds));
            });
        }

        if ($request->get('characters')) {
            $characterIds = explode(',', $request->get('characters'));

            $queryBuilder->whereHas('comics.characters', function ($query) use ($characterIds) {
                $query->latest()->whereIn('character_id', getValidIds($characterIds));
            });
        }

        if ($request->get('series')) {
            $seriesIds = explode(',', $request->get('series'));

            $queryBuilder->whereHas('comics', function ($query) use ($seriesIds) {
                $query->whereIn('serie_id', getValidIds($seriesIds));
            });
        }  

        if ($request->get('comics')) {
            $comicIds = explode(',', $request->get('comics'));

            $queryBuilder->whereHas('comics', function ($query) use ($comicIds) {
                $query->whereIn('id', getValidIds($comicIds));
            });
        }
              
        if ($request->get('stories')) {
            $storyIds = explode(',', $request->get('stories'));

            $queryBuilder->whereHas('comics.stories', function ($query) use ($storyIds) {
                $query->latest()->whereIn('story_id', getValidIds($storyIds));
            });
        }

        if ($request->get('orderBy')) {
            $queryBuilder->myOrderBy($request->get('orderBy'));
        } else {
            $queryBuilder->orderBy('id');
        }
    }

    /**
     * Apply ordering from a request
     */
    public function scopeMyOrderBy($query, $orderBytext)
    {
        return ApplyQueryOrderBy::eventOrderBy($query, $orderBytext);
    }
}
