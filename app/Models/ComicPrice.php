<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComicPrice extends Model
{
    public $timestamps = false;
    protected $hidden = ['id','comic_id'];

    protected $casts = [
        'price' => 'float',
    ];
}
