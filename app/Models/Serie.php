<?php

namespace App\Models;

use App\Enums\Enums\ComicFormatEnum;
use App\Http\Requests\SeriesRequest;
use App\Models\Queries\ApplyQueryOrderBy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
require_once app_path('/helpers.php');

class Serie extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;

    const UPDATED_AT = 'modified_at';
    const CREATED_AT = 'created_at_series';

    protected $hidden = ['laravel_through_key', 'pivot'];

    /**
     * Relation NxN with urls
     */
    public function urls()
    {
        return $this->belongsToMany(Url::class, 'series_urls', 'serie_id', 'url_id');
    }

    public function previous()
    {
        return $this->hasOne(Serie::class, 'id', 'previous_id');
    }

    public function next()
    {
        return $this->hasOne(Serie::class, 'id', 'next_id');
    }

    public function comics()
    {
        return $this->hasMany(Comic::class);
    }

    public function characters()
    {
        return $this->hasManyDeepFromRelations($this->comics(), (new Comic)->characters())->distinct(DB::raw('character_id, serie_id'));
    }

    public function creators()
    {
        return $this->hasManyDeepFromRelations($this->comics(), (new Comic)->creators())->distinct(DB::raw('creator_id, serie_id'));
    }

    public function events()
    {
        return $this->hasManyDeepFromRelations($this->comics(), (new Comic)->events())->distinct(DB::raw('event_id, serie_id'));
    }

    public function stories()
    {
        return $this->hasManyDeepFromRelations($this->comics(), (new Comic)->stories())->distinct(DB::raw('story_id, serie_id'));
    }

    /**
     * Apply all relations to return complete data
     */
    public function scopeApplyRelations($scopedQuery)
    {
        return $scopedQuery->with([
            'comics' => function ($query) {
                $query->selectRaw('comics.id, comics.title, comics.serie_id');
                $query->orderBy('comics.id');
                $query->latest()->limit(20);
            },
            'stories' => function ($query) {
                $query->selectRaw('stories.id, stories.title');
                $query->orderBy('stories.id');
                $query->latest()->limit(20);
            },
            'events' => function ($query) {
                $query->selectRaw('events.id, events.title');
                $query->orderBy('events.id');
                $query->latest()->limit(20);
            },
            'creators' => function ($query) {
                $query->selectRaw('creators.id, creators.first_name, creators.middle_name, creators.last_name');
                $query->orderBy('creators.id');
                $query->latest()->limit(20);
            },
            'characters' => function ($query) {
                $query->selectRaw('characters.id, characters.name');
                $query->orderBy('characters.id');
                $query->latest()->limit(20);
            },
            'urls'
        ])
            ->withCount([
                'comics',
                'stories' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(story_id))'));
                },
                'events' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(event_id))'));
                },
                'creators' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(creator_id))'));
                },
                'characters' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(character_id))'));
                }
            ]);
    }

    /**
     * Apply filters from a request
     */
    public function scopeApplyFilters($queryBuilder, SeriesRequest $request, $character_id)
    {
        //apply character id filter
        $queryBuilder->whereHas('characters', function ($query) use ($character_id) {
            $query->latest()->where('character_id', $character_id);
        });

        if ($request->get('title')) {
            $queryBuilder->where(DB::raw('lower(title)'), strtolower($request->get('title')));
        }
        if ($request->get('titleStartsWith')) {
            $queryBuilder->whereRaw('lower(title) ilike ?', [strtolower($request->get('nameStartsWith')) . '%']);
        }
        if ($request->get('modifiedSince') && getValidDate($request->get('modifiedSince'))) {
            $queryBuilder->where('modified_at', '>=', getValidDate($request->get('modifiedSince')));
        }

        if ($request->get('seriesType')) {
            $queryBuilder->where(DB::raw('lower(type)'), strtolower($request->get('seriesType')));
        }

        if ($request->get('contains')) {
            $comicFormats = explode(',', $request->get('contains'));

            //get only valid values
            $comicFormats = array_intersect($comicFormats, ComicFormatEnum::getValues());

            $queryBuilder->whereHas('comics', function ($query) use ($comicFormats) {
                $query->whereIn('format', $comicFormats);
            });
        }

        if ($request->get('comics')) {
            $comicIds = explode(',', $request->get('comics'));

            $queryBuilder->whereHas('comics', function ($query) use ($comicIds) {
                $query->whereIn('id', getValidIds($comicIds));
            });
        }

        if ($request->get('stories')) {
            $storyIds = explode(',', $request->get('stories'));

            $queryBuilder->whereHas('comics.stories', function ($query) use ($storyIds) {
                $query->latest()->whereIn('story_id', getValidIds($storyIds));
            });
        }

        if ($request->get('events')) {
            $eventIds = explode(',', $request->get('events'));

            $queryBuilder->whereHas('comics.events', function ($query) use ($eventIds) {
                $query->latest()->whereIn('event_id', getValidIds($eventIds));
            });
        }

        if ($request->get('creators')) {
            $creatorIds = explode(',', $request->get('creators'));

            $queryBuilder->whereHas('comics.creators', function ($query) use ($creatorIds) {
                $query->latest()->whereIn('creator_id', getValidIds($creatorIds));
            });
        }

        if ($request->get('characters')) {
            $characterIds = explode(',', $request->get('characters'));

            $queryBuilder->whereHas('comics.characters', function ($query) use ($characterIds) {
                $query->latest()->whereIn('character_id', getValidIds($characterIds));
            });
        }             

        if ($request->get('orderBy')) {
            $queryBuilder->myOrderBy($request->get('orderBy'));
        } else {
            $queryBuilder->orderBy('id');
        }
    }

    /**
     * Apply ordering from a request
     */
    public function scopeMyOrderBy($query, $orderBytext)
    {
        return ApplyQueryOrderBy::serieOrderBy($query, $orderBytext);
    }
}
