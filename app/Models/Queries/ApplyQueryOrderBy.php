<?php

namespace App\Models\Queries;

require_once app_path('/helpers.php');

abstract class ApplyQueryOrderBy
{
    public static function defaultOrderBy($query, $orderBytext)
    {
        $orderList = explode(',', $orderBytext);

        $orderList = ApplyQueryOrderBy::mapOrderType($orderList);

        $query->orderByRaw(implode(",", $orderList));
    }

    public static function eventOrderBy($query, $orderBytext)
    {
        $orderBytext = str_replace('name', 'title', $orderBytext);
        $orderBytext = str_replace('startDate', 'start', $orderBytext);

        $orderList = explode(',', $orderBytext);

        $orderList = ApplyQueryOrderBy::mapOrderType($orderList);

        $query->orderByRaw(implode(",", $orderList));
    }

    public static function serieOrderBy($query, $orderBytext)
    {
        $orderBytext = str_replace('startYear', 'start_year', $orderBytext);

        $orderList = explode(',', $orderBytext);

        $orderList = ApplyQueryOrderBy::mapOrderType($orderList);

        $query->orderByRaw(implode(",", $orderList));
    }

    public static function comicOrderBy($query, $orderBytext)
    {
        $orderList = explode(',', $orderBytext);

        $orderList = ApplyQueryOrderBy::mapOrderType($orderList, function ($term) {
            //todo Ver como implementar este trecho
            if (strpos((string)$term, 'focDate' === 0)) {
                return '';
            } else if (strpos((string)$term, 'onsaleDate' === 0)) {
                return '';
            }
        });

        $query->orderByRaw(implode(",", $orderList));
    }

    private static function mapOrderType($orderList, $mapCallable = null)
    {
        return array_map(function ($orderTerm){
            $orderTerm = camelCaseToSnakeCase($orderTerm);
            if (strpos($orderTerm, 'modified') >= 0) {
                $orderTerm = str_replace('modified', 'modified_at', $orderTerm);
            }           

            if (strpos($orderTerm, '-') === 0) {
                return substr($orderTerm, 1) . ' desc';
            }

            return $orderTerm;
        }, $orderList);
    }
}
