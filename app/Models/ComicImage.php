<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComicImage extends Model
{
    public $timestamps = false;
    protected $hidden = ['id','comic_id'];
}
