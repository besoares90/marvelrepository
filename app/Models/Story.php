<?php

namespace App\Models;

use App\Http\Requests\StoriesRequest;
use App\Models\Queries\ApplyQueryOrderBy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
require_once app_path('/helpers.php');

class Story extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;

    const UPDATED_AT = 'modified_at';
    const CREATED_AT = 'created_at_stories';

    protected $hidden = ['laravel_through_key', 'pivot'];

    public function comics()
    {
        return $this->belongsToMany(Comic::class, 'comics_stories', 'story_id', 'comic_id');
    }

    public function series()
    {
        return $this->hasManyDeepFromRelations($this->comics(), (new Comic)->serie())->distinct(DB::raw('serie_id, story_id'));
    }

    public function events()
    {
        return $this->hasManyDeep(Event::class, ['comics_stories', Comic::class, 'comics_events'])->distinct(DB::raw('event_id, story_id'));
    }  

    public function creators()
    {
        return $this->hasManyDeep(Creator::class, ['comics_stories', Comic::class, 'comics_creators'])->distinct(DB::raw('creator_id, story_id'));
    }

    public function characters()
    {
        return $this->hasManyDeep(Character::class, ['comics_stories', Comic::class, 'comics_characters'])->distinct(DB::raw('character_id, story_id'));
    }

    /**
     * Apply all relations to return complete data
     */
    public function scopeApplyRelations($scopedQuery)
    {
        return $scopedQuery->with([
            'comics' => function ($query) {
                $query->selectRaw('comics.id, comics.title');
                $query->orderBy('comics.id');
                $query->latest()->limit(20);
            },
            'series' => function ($query) {
                $query->selectRaw('series.id, series.title');
                $query->orderBy('series.id');
                $query->latest()->limit(20);
            },
            'events' => function ($query) {
                $query->selectRaw('events.id, events.title');
                $query->orderBy('events.id');
                $query->latest()->limit(20);
            }, 
            'creators' => function ($query) {
                $query->selectRaw('creators.id, creators.first_name, creators.middle_name, creators.last_name');
                $query->orderBy('creators.id');
                $query->latest()->limit(20);
            },
            'characters' => function ($query) {
                $query->selectRaw('characters.id, characters.name');
                $query->orderBy('characters.id');
                $query->latest()->limit(20);
            },                                              
        ])
            ->withCount([
                'comics',
                'series' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(serie_id))'));
                },
                'events' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(event_id))'));
                },
                'creators' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(creator_id))'));
                },
                'characters' => function ($query) {
                    $query->latest()->select(DB::raw('count(distinct(character_id))'));
                },                
            ]);
    }

    /**
     * Apply filters from a request
     */
    public function scopeApplyFilters($queryBuilder, StoriesRequest $request, $character_id)
    {
        //apply character id filter
        $queryBuilder->whereHas('characters', function ($query) use ($character_id) {
            $query->latest()->where('character_id', $character_id);
        });

        if ($request->get('modifiedSince') && getValidDate($request->get('modifiedSince'))) {
            $queryBuilder->where('modified_at', '>=', getValidDate($request->get('modifiedSince')));
        }

        if ($request->get('comics')) {
            $comicIds = explode(',', $request->get('comics'));

            $queryBuilder->whereHas('comics', function ($query) use ($comicIds) {
                $query->whereIn('id', getValidIds($comicIds));
            });
        }

        if ($request->get('series')) {
            $seriesIds = explode(',', $request->get('series'));

            $queryBuilder->whereHas('comics', function ($query) use ($seriesIds) {
                $query->whereIn('serie_id', getValidIds($seriesIds));
            });
        }  

        if ($request->get('events')) {
            $eventsIds = explode(',', $request->get('events'));

            $queryBuilder->whereHas('comics.events', function ($query) use ($eventsIds) {
                $query->latest()->whereIn('event_id', getValidIds($eventsIds));
            });
        }

        if ($request->get('creators')) {
            $creatorIds = explode(',', $request->get('creators'));

            $queryBuilder->whereHas('comics.creators', function ($query) use ($creatorIds) {
                $query->latest()->whereIn('creator_id', getValidIds($creatorIds));
            });
        }

        if ($request->get('characters')) {
            $characterIds = explode(',', $request->get('characters'));

            $queryBuilder->whereHas('comics.characters', function ($query) use ($characterIds) {
                $query->latest()->whereIn('character_id', getValidIds($characterIds));
            });
        }

        if ($request->get('orderBy')) {
            $queryBuilder->myOrderBy($request->get('orderBy'));
        } else {
            $queryBuilder->orderBy('id');
        }
    }

    /**
     * Apply ordering from a request
     */
    public function scopeMyOrderBy($query, $orderBytext)
    {
        return ApplyQueryOrderBy::defaultOrderBy($query, $orderBytext);
    }

    public function scopeAsChild($query)
    {
        $query->selectRaw('id, title');
        $query->orderBy('comics.id');
    }
}
