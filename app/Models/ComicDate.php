<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComicDate extends Model
{
    public $timestamps = false;
    protected $hidden = ['id', 'comic_id'];

    public function comic(){
        return $this->belongsTo(Comic::class);
    }
}
