<?php

namespace App\Models;

use App\Enums\Enums\DateDescriptorEnum;
use App\Http\Requests\ComicsRequest;
use App\Models\Queries\ApplyQueryOrderBy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Comic extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;

    protected $hidden = ['created_at', 'laravel_through_key', 'pivot'];
    protected $casts = [
        'issue_number' => 'integer'
    ];
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';

    /**
     * Relation NxN with urls
     */
    public function urls()
    {
        return $this->belongsToMany(Url::class, 'comics_urls', 'comic_id', 'url_id');
    }

    /**
     * Relation NxN with stories
     */
    public function stories()
    {
        return $this->belongsToMany(Story::class, 'comics_stories', 'comic_id', 'story_id');
    }

    /**
     * Relation NxN with events
     */
    public function events()
    {
        return $this->belongsToMany(Event::class, 'comics_events', 'comic_id', 'event_id');
    }

    /**
     * Relation NxN with creators
     */
    public function creators()
    {
        return $this->belongsToMany(Creator::class, 'comics_creators', 'comic_id', 'creator_id');
    }

    /**
     * Relation NxN with characters
     */
    public function characters()
    {
        return $this->belongsToMany(Character::class, 'comics_characters', 'comic_id', 'character_id'); //->select(['id']);
    }

    /**
     * Relation Nx1 with series
     */
    public function serie()
    {
        return $this->belongsTo(Serie::class);
    }

    /**
     * Relation 1xN with comic_dates
     */
    public function dates()
    {
        return $this->hasMany(ComicDate::class);
    }

    /**
     * Relation 1xN with comic_images
     */
    public function images()
    {
        return $this->hasMany(ComicImage::class);
    }

    /**
     * Relation 1xN with comic_prices
     */
    public function prices()
    {
        return $this->hasMany(ComicPrice::class);
    }

    /**
     * Relation 1xN with comic_txt_objects
     */
    public function text_objects()
    {
        return $this->hasMany(ComicTextObject::class);
    }

    /**
     * Relation 1xN for variants
     */
    public function variants()
    {
        return $this->belongsToMany(Comic::class, 'comics_variants', 'comic_id', 'variant_id');
    }

    /**
     * Relation NxN for collections
     */
    public function collections()
    {
        return $this->belongsToMany(Comic::class, 'comics_collections', 'comic_id', 'collection_id');
    }

    /**
     * Relation NxN for collected_issues
     */
    public function collected_issues()
    {
        return $this->belongsToMany(Comic::class, 'comics_collected_issues', 'comic_id', 'collected_issue_id');
    }
    /**
     * Apply all relations to return complete data
     */
    public function scopeApplyRelations($scopedQuery)
    {
        return $scopedQuery->with([
            'serie',
            'variants',
            'collections',
            'collected_issues',
            'dates',
            'prices',
            'images',
            'creators' => function ($query) {
                $query->selectRaw('id, first_name, middle_name, last_name');
                $query->orderBy('creators.id');
                $query->latest()->limit(20);
            },
            'stories' => function ($query) {
                $query->selectRaw('stories.id, stories.title');
                $query->orderBy('stories.id');
                $query->latest()->limit(20);
            },
            'events' => function ($query) {
                $query->selectRaw('events.id, events.title');
                $query->orderBy('events.id');
                $query->latest()->limit(20);
            },
            'urls'
        ])
            ->withCount([
                'creators',
                'stories',
                'events',
                'creators',
            ]);
    }

    /**
     * Apply filters from a request
     */
    public function scopeApplyFilters($queryBuilder, ComicsRequest $request, $character_id)
    {
        //apply character id filter
        $queryBuilder->whereHas('characters', function ($query) use ($character_id) {
            $query->where('id', $character_id);
        });

        if ($request->get('format')) {
            $queryBuilder->where('format', $request->get('format'));
        }

        //Não consegui compreender o que este filtro faz
        //Todo: verificar
        if ($request->get('formatType')) {
        }

        //Não consegui compreender o que este filtro faz também
        //Todo: verificar
        if ($request->get('noVariants')) {
        }

        if ($request->get('dateDescriptor')) {
            switch ($request->get('dateDescriptor')) {
                case DateDescriptorEnum::LASTWEEK:
                    $queryBuilder->whereHas('dates', function ($query) {
                        $query->where('date', '>=', date("Y-m-d", strtotime("last week sunday")));
                    });
                    break;
                case DateDescriptorEnum::THISWEEK:
                    $queryBuilder->whereHas('dates', function ($query) {
                        $query->where('date', '>=', date("Y-m-d", strtotime("this week sunday")));
                    });
                    break;
                case DateDescriptorEnum::NEXTWEEK:
                    $queryBuilder->whereHas('dates', function ($query) {
                        $query->where('date', '>=', date("Y-m-d", strtotime("next week sunday")));
                    });
                    break;
                case DateDescriptorEnum::THISMONTH:
                    $queryBuilder->whereHas('dates', function ($query) {
                        $query->where('date', '>=', date("Y-m-01"));
                    });
                    break;
            }
        }

        if ($request->get('dateRange')) {
            $validDate = getValidDate($request->get('dateRange'));
            if (!!$validDate) {
                $validDates = explode(',', $validDate);

                $queryBuilder->whereHas('dates', function ($query) use ($validDates) {
                    $query->where(function ($innerQuery) use ($validDates) {
                        $innerQuery->where('date', '>=', $validDates[0]);
                        $innerQuery->where('date', '<=', $validDates[1]);
                    });
                });
            }
        }

        if ($request->get('title')) {
            $queryBuilder->where(DB::raw('lower(title)'), strtolower($request->get('title')));
        }

        if ($request->get('titleStartsWith')) {
            $queryBuilder->whereRaw('lower(title) ilike ?', [strtolower($request->get('titleStartsWith')) . '%']);
        }

        if ($request->get('startYear')) {
            $queryBuilder->whereRaw('title ilike ?', ['%(' . $request->get('startYear') . ')%']);
        }

        if ($request->get('issueNumber')) {
            $queryBuilder->where('issue_number', $request->get('issueNumber'));
        }

        if ($request->get('digitalId')) {
            if (!intval($request->get('digitalId'))) {
                $queryBuilder->where(DB::raw('cast (digital_id as text)'), $request->get('digitalId'));
            } else {
                $queryBuilder->where('digital_id', $request->get('digitalId'));
            }
        }

        if ($request->get('diamondCode')) {
            $queryBuilder->where('diamond_code', $request->get('diamondCode'));
        }

        if ($request->get('upc')) {
            $queryBuilder->where('upc', $request->get('upc'));
        }

        if ($request->get('isbn')) {
            $queryBuilder->where('isbn', $request->get('isbn'));
        }

        if ($request->get('ean')) {
            $queryBuilder->where('ean', $request->get('ean'));
        }

        if ($request->get('issn')) {
            $queryBuilder->where('issn', $request->get('issn'));
        }

        if ($request->get('hasDigitalIssue')) {
            $queryBuilder->where(function ($query) {
                $query->where('digital_id', '<>', 0);
                $query->whereNotNull('digital_id');
            });
        }

        if ($request->get('modifiedSince') && getValidDate($request->get('modifiedSince'))) {
            $queryBuilder->where('modified_at', '>=', getValidDate($request->get('modifiedSince')));
        }

        if ($request->get('creators')) {
            $creatorIds = explode(',', $request->get('creators'));

            $queryBuilder->whereHas('creators', function ($query) use ($creatorIds) {
                $query->whereIn('id', getValidIds($creatorIds));
            });
        }

        if ($request->get('characters')) {
            $characterIds = explode(',', $request->get('characters'));

            $queryBuilder->whereHas('characters', function ($query) use ($characterIds) {
                $query->whereIn('id', getValidIds($characterIds));
            });
        }

        if ($request->get('series')) {
            $seriesIds = explode(',', $request->get('series'));

            $queryBuilder->whereIn('serie_id', getValidIds($seriesIds));
        }

        if ($request->get('events')) {
            $eventIds = explode(',', $request->get('events'));

            $queryBuilder->whereHas('events', function ($query) use ($eventIds) {
                $query->latest()->whereIn('event_id', getValidIds($eventIds));
            });
        }

        if ($request->get('stories')) {
            $storyIds = explode(',', $request->get('stories'));

            $queryBuilder->whereHas('stories', function ($query) use ($storyIds) {
                $query->latest()->whereIn('story_id', getValidIds($storyIds));
            });
        }

        if ($request->get('sharedAppearances')) {
            $characterIds = explode(',', $request->get('sharedAppearances'));

            $queryBuilder->where(function ($queryInner) use ($characterIds) {
                foreach ($characterIds as $character_id) {
                    $queryInner->whereHas('characters', function ($query) use ($character_id) {
                        $query->where('id', $character_id);
                    });
                }
            });
        }

        if ($request->get('collaborators')) {
            $creatorIds = explode(',', $request->get('collaborators'));

            $queryBuilder->where(function ($queryInner) use ($creatorIds) {
                foreach ($creatorIds as $creator_id) {
                    $queryInner->whereHas('creators', function ($query) use ($creator_id) {
                        $query->where('id', $creator_id);
                    });
                }
            });
        }

        if ($request->get('orderBy')) {
            $queryBuilder->myOrderBy($request->get('orderBy'));
        } else {
            $queryBuilder->orderBy('id');
        }
    }

    /**
     * Apply ordering from a request
     */
    public function scopeMyOrderBy($query, $orderBytext)
    {
        return ApplyQueryOrderBy::comicOrderBy($query, $orderBytext);
    }
}
