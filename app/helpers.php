<?php

//Validate if $value is a list of numbers comma separated
if (!function_exists('validateRegexIdsCommaSeparated')) {
    function validateRegexIdsCommaSeparated($value)
    {
        return preg_match('/^\d+(,\d+)*$/', $value);
    }
}

if (!function_exists('getValidIds')) {
    function getValidIds($terms)
    {
        $data = [];
        foreach($terms as $term){
            if (intval($term)){
                $data[] = $term;
            }
        }

        return $data;
    }
}

if (!function_exists('validateDateRange')) {
    function validateDateRange($value)
    {
        return preg_match('/^\w+(,)\w+$/', $value);
    }
}

/**
 * Format a valid date for parameters of type Date
 * Ex: If received a date like '2020-10', converts to '2020-10-01', preventing a database error
 */
if (!function_exists('getValidDate')) {
    function getValidDate($dateString)
    {
        if (!$dateString) return false;
        
        $dateString = str_replace('/', '-', $dateString);

        //regex for full valid date (YYYY-MM-DD)
        if (preg_match('/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/', $dateString)) {
            return $dateString;
        }

        //regex for (YY-MM)
        if (preg_match('/([12]\d{3}-(0[1-9]|1[0-2]))$/', $dateString)) {
            return $dateString . '-01';
        }

        return false;
    }
}

if (!function_exists('camelCaseToSnakeCase')) {
    function camelCaseToSnakeCase($string)
    {
        return substr(strtolower(preg_replace('/[A-Z]/', '_$0', $string )), 0);
    }
}
