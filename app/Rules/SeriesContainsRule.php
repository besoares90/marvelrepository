<?php

namespace App\Rules;

use App\Enums\Enums\ComicFormatEnum;
use Illuminate\Contracts\Validation\Rule;

class SeriesContainsRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return in_array($value, ComicFormatEnum::getValues());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You must pass at least one valid type if you set the seriesType filter.';
    }
}
