<?php

namespace App\Rules;

use App\Enums\Enums\DateDescriptorEnum;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Exceptions\HttpResponseException;

class DateDescriptorRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!in_array($value, DateDescriptorEnum::getValues()))
            throw new HttpResponseException(response()->json([
                'code' => 500,
                'status' => "Internal Server Error"
            ], 500));

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You must pass at least one valid type if you set the dateDescriptor filter.';
    }
}
