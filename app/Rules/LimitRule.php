<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class LimitRule implements Rule
{
    private $error;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (intval($value) <= 0) {
            $this->error = 'You must pass an integer limit greater than 0.';
            return false;
        } else if (intval($value) > 100) {
            $this->error = 'You may not request more than 100 items.';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->error;
    }
}
