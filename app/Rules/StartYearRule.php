<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StartYearRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $numberValue = intval($value);

        return !!$numberValue && strlen((string)$value) === 4;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You must pass a four-digit number if you set the series year filter.';
    }
}
