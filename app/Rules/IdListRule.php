<?php

namespace App\Rules;

require_once app_path('/helpers.php');

use App\Enums\Enums\EntityListRuleEnum;
use Illuminate\Contracts\Validation\Rule;

class IdListRule implements Rule
{
    private $entity;
    private $error;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $hasOneValid = false;
        $terms = explode(',', $value);

        if (sizeof(explode(',', $value)) > 10) {
            $this->error = 'size';
            return false;
        }

        foreach($terms as $term){
            if (intval($term)){
                return true;
            }
        }

        // if (!validateRegexIdsCommaSeparated($value)){
        //     $this->error = 'regex';
        //     return false;
        // } else if (sizeof(explode(',', $value)) > 10){
        //     $this->error = 'size';
        //     return false;
        // }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if ($this->error === 'regex') {
            if ($this->entity == EntityListRuleEnum::SHAREDAPPEARANCES) {
                return 'You must pass at least one valid character if you set the shared appearances filter.';
            } else if ($this->entity == EntityListRuleEnum::COLLABORATORS) {
                return 'You must pass at least one valid creator if you set the collaborators filter.';
            }

            return 'You must pass at least one valid ' . $this->entity . ' id if you set the issue filter.';
        } else if ($this->error === 'size') {
            return 'You may not submit more than 10 ' . $this->entity . ' ids.';
        }
    }
}
