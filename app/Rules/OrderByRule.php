<?php

namespace App\Rules;

use App\Enums\Enums\CharacterOrderByEnum;
use App\Enums\Enums\ComicOrderByEnum;
use App\Enums\Enums\CreatorOrderByEnum;
use App\Enums\Enums\EntityListEnum;
use App\Enums\Enums\EventOrderByEnum;
use App\Enums\Enums\SerieOrderByEnum;
use App\Enums\Enums\StoryOrderByEnum;
use Illuminate\Contracts\Validation\Rule;

require_once app_path('/helpers.php');

class OrderByRule implements Rule
{
    private $entity;
    private $wrongValue;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $values = explode(',', $value);
        $validateValues = [];

        switch ($this->entity){
            case EntityListEnum::CHARACTER:
                $validateValues = CharacterOrderByEnum::getValues();
            break;

            case EntityListEnum::COMIC:
                $validateValues = ComicOrderByEnum::getValues();
            break;

            case EntityListEnum::CREATOR:
                $validateValues = CreatorOrderByEnum::getValues();
            break;

            case EntityListEnum::EVENT:
                $validateValues = EventOrderByEnum::getValues();
            break;

            case EntityListEnum::SERIES:
                $validateValues = SerieOrderByEnum::getValues();
            break;

            case EntityListEnum::STORY:
                $validateValues = StoryOrderByEnum::getValues();
            break;
        }
        
        foreach ($values as $value){
            if (!in_array($value, $validateValues)){
                $this->wrongValue = $value;
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->wrongValue.' is not a valid ordering parameter.';
    }
}
