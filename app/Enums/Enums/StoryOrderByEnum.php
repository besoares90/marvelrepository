<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class StoryOrderByEnum extends Enum
{
    const ID = 'id';
    const NAME_DESC = '-id';
    const MODIFIED = 'modified';
    const MODIFIED_DESC = '-modified';
}
