<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class SeriesParametersEnum extends Enum
{
    const title = 'title';
    const titleStartsWith = 'titleStartsWith';
    const startYear = 'startYear';
    const modifiedSince = 'modifiedSince';

    const comics = 'comics';
    const stories = 'stories';
    const events = 'events';
    const creators = 'creators';
    const characters = 'characters';
    const seriesType = 'seriesType';
    const contains = 'contains';

    const offset = 'offset';
    const limit = 'limit';
    const orderBy = 'orderBy';
}
