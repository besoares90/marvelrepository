<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class CharacterOrderByEnum extends Enum
{
    const NAME = 'name';
    const NAME_DESC = '-name';
    const MODIFIED = 'modified';
    const MODIFIED_DESC = '-modified';
}
