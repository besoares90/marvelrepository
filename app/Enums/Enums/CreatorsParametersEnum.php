<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class CreatorsParametersEnum extends Enum
{
    const firstName = 'firstName';
    const middleName = 'middleName';
    const lastName = 'lastName';
    const suffix = 'suffix';
    const nameStartsWith = 'nameStartsWith';
    const firstNameStartsWith = 'firstNameStartsWith';
    const middleNameStartsWith = 'middleNameStartsWith';

    const lastNameStartsWith = 'lastNameStartsWith';
    const modifiedSince = 'modifiedSince';
    const comics = 'comics';
    const series = 'series';
    const events = 'events';
    const stories = 'stories';

    const offset = 'offset';
    const limit = 'limit';
    const orderBy = 'orderBy';
}
