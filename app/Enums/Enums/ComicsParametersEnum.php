<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class ComicsParametersEnum extends Enum
{
    const format = 'format';
    const formatType = 'formatType';
    const noVariants = 'noVariants';
    const dateDescriptor = 'dateDescriptor';
    const dateRange = 'dateRange';
    const title = 'title';
    const titleStartsWith = 'titleStartsWith';
    const startYear = 'startYear';
    const issueNumber = 'issueNumber';
    const diamondCode = 'diamondCode';
    const digitalId = 'digitalId';
    const upc = 'upc';
    const isbn = 'isbn';
    const ean = 'ean';
    const issn = 'issn';
    const hasDigitalIssue = 'hasDigitalIssue';
    const modifiedSince = 'modifiedSince';
    const sharedAppearances = 'sharedAppearances';
    const collaborators = 'collaborators';

    const creators = 'creators';
    const characters = 'characters';
    const series = 'series';
    const events = 'events';
    const stories = 'stories';

    const seriesType = 'seriesType';
    const contains = 'contains';

    const offset = 'offset';
    const limit = 'limit';
    const orderBy = 'orderBy';
}
