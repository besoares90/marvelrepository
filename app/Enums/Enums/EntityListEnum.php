<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

/**
 */
final class EntityListEnum extends Enum
{
    const CREATOR = 'creator';
    const CHARACTER = 'character';
    const COMIC = 'issue';
    const COMIC_ISSUE = 'comic_issue';
    const EVENT = 'event';
    const SERIES = 'series';
    const STORY = 'story';

    const CREATOR_PLURAL = 'creators';
    const CHARACTER_PLURAL = 'characters';
    const COMIC_PLURAL = 'comics';
    const COMIC_ISSUE_PLURAL = 'comic_issues';
    const EVENT_PLURAL = 'events';
    const SERIES_PLURAL = 'series';
    const STORY_PLURAL = 'stories';
}
