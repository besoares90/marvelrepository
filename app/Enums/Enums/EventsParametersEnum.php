<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class EventsParametersEnum extends Enum
{
    const name = 'name';
    const nameStartsWith = 'nameStartsWith';
    const modifiedSince = 'modifiedSince';

    const characters = 'characters';
    const creators = 'creators';
    const series = 'series';
    const comics = 'comics';
    const stories = 'stories';

    const offset = 'offset';
    const limit = 'limit';
    const orderBy = 'orderBy';
}
