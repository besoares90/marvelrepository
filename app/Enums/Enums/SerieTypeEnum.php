<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class SerieTypeEnum extends Enum
{
    const COLLECTION = 'collection';
    const ONESHOT = 'one shot';
    const LIMITED = 'limited';
    const ONGOING = 'ongoing';
}
