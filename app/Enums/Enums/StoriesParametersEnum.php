<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class StoriesParametersEnum extends Enum
{
    const modifiedSince = 'modifiedSince';

    const comics = 'comics';
    const series = 'series';
    const events = 'events';
    const creators = 'creators';
    const characters = 'characters';
    const contains = 'contains';

    const offset = 'offset';
    const limit = 'limit';
    const orderBy = 'orderBy';
}
