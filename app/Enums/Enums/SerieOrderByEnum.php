<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class SerieOrderByEnum extends Enum
{
    const NAME = 'title';
    const NAME_DESC = '-title';
    const STARTDATE = 'startYear';
    const STARTDATE_DESC = '-startYear';
    const MODIFIED = 'modified';
    const MODIFIED_DESC = '-modified';
}
