<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class CreatorOrderByEnum extends Enum
{
    const LASTNAME = 'lastName';
    const LASTNAME_DESC = '-lastName';
    const FIRSTNAME = 'firstName';
    const FIRSTNAME_DESC = '-firstName';
    const MIDDLENAME = 'middleName';
    const MIDDLENAME_DESC = '-middleName';
    const SUFFIX = 'suffix';
    const SUFFIX_DESC = '-suffix';
    const MODIFIED = 'modified';
    const MODIFIED_DESC = '-modified';
}
