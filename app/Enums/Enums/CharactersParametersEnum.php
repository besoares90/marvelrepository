<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class CharactersParametersEnum extends Enum
{
    const NAME = 'name';
    const NAMESTARTSWITH = 'nameStartsWith';
    const MODIFIEDSINCE = 'modifiedSince';
    const COMICS = 'comics';
    const SERIES = 'series';
    const EVENTS = 'events';
    const STORIES = 'stories';

    const OFFSET = 'offset';
    const LIMIT = 'limit';
    const ORDERBY = 'orderBy';
}
