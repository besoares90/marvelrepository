<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

/**
 */
final class EntityListRuleEnum extends Enum
{
    const CREATOR = 'creator';
    const CHARACTER = 'character';
    const COMIC = 'issue';
    const COLLABORATORS = 'collaborators';
    const EVENT = 'event';
    const ISSUE = 'issue';
    const SERIES = 'series';
    const SHAREDAPPEARANCES = 'sharedAppearances';
    const STORY = 'story';
}
