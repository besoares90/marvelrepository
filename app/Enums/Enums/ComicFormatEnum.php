<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class ComicFormatEnum extends Enum
{
    const COMIC = 'comic';
    const MAGAZINE = 'magazine';
    const TRADER_PAPERBACK = 'trader paperback';
    const HARDCOVER = 'hardcover';
    const DIGEST = 'digest';
    const GRAPHIC_NOVEL = 'graphic novel';
    const DIGITAL_COMIC = 'digital comic';
    const INFINITE_COMIC = 'infinite comic';
}
