<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class ComicOrderByEnum extends Enum
{
    const FOCDATE = 'focDate';
    const FOCDATE_DESC = '-focDate';
    const ONSALEDATE = 'onSaleDate';
    const ONSALEDATE_DESC = '-onSaleDate';
    const TITLE = 'title';
    const TITLE_DESC = '-title';
    const ISSUENUMBER = 'issueNumber';
    const ISSUENUMBER_DESC = '-issueNumber';
    const MODIFIED = 'modified';
    const MODIFIED_DESC = '-modified';
}
