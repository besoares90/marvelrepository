<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class EventOrderByEnum extends Enum
{
    const NAME = 'name';
    const NAME_DESC = '-name';
    const STARTDATE = 'startDate';
    const STARTDATE_DESC = '-startDate';
    const MODIFIED = 'modified';
    const MODIFIED_DESC = '-modified';
}
