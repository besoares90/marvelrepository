<?php

namespace App\Enums\Enums;

use BenSampo\Enum\Enum;

final class DateDescriptorEnum extends Enum
{
    const LASTWEEK = 'lastWeek';
    const THISWEEK = 'thisWeek';
    const NEXTWEEK = 'nextWeek';
    const THISMONTH = 'thisMonth';   
}
