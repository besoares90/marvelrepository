<?php

namespace App\Http\Controllers;

use App\Enums\Enums\EntityListEnum;
use App\Http\Requests\CharactersRequest;
use App\Http\Requests\ComicsRequest;
use App\Http\Requests\CreatorsRequest;
use App\Http\Requests\EventsRequest;
use App\Http\Requests\SeriesRequest;
use App\Http\Requests\StoriesRequest;
use App\Http\Resources\Collections\CharacterResourceCollection;
use App\Http\Resources\Collections\ComicResourceCollection;
use App\Http\Resources\Collections\CreatorResourceCollection;
use App\Http\Resources\Collections\EventResourceCollection;
use App\Http\Resources\Collections\SerieResourceCollection;
use App\Http\Resources\Collections\StoryResourceCollection;
use App\Models\Character;
use App\Models\Comic;
use App\Models\Creator;
use App\Models\Event;
use App\Models\Serie;
use App\Models\Story;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class CharacterController extends Controller
{
    public function get(CharactersRequest $request)
    {
        $offset = $request->get('offset') ? intval($request->get('offset')) : 0;
        $limit = $request->get('limit') ? intval($request->get('limit')) : 20;

        $characters = Character::applyRelations()->applyFilters($request);

        $total = $characters->count();
        $characters = $characters->skip($offset)->take($limit)->get();

        return CharacterResourceCollection::make($characters)->init($offset, $limit, $total);
    }

    public function getById(Request $request, $character_id)
    {        
        $offset = $request->get('offset') ? intval($request->get('offset')) : 0;
        $limit = $request->get('limit') ? intval($request->get('limit')) : 20;

        $characters = Character::applyRelations();
        $characters = $characters->where('id', $character_id)->take(1)->get();

        if (!sizeof($characters)){
            throw new ResourceNotFoundException(EntityListEnum::CHARACTER);
        }

        return CharacterResourceCollection::make($characters)->init($offset, $limit, sizeof($characters));
    }

    public function getCharacterStories(StoriesRequest $request, $character_id)
    {
        $offset = $request->get('offset') ? intval($request->get('offset')) : 0;
        $limit = $request->get('limit') ? intval($request->get('limit')) : 20;

        $stories = Story::applyRelations()->applyFilters($request, $character_id);

        $total = $stories->count();
        $stories = $stories->skip($offset)->take($limit)->get();

        return StoryResourceCollection::make($stories)->init($offset, $limit, $total);
    }

    public function getCharacterComics(ComicsRequest $request, $character_id)
    {
        $offset = $request->get('offset') ? intval($request->get('offset')) : 0;
        $limit = $request->get('limit') ? intval($request->get('limit')) : 20;

        $comics = Comic::applyRelations()->applyFilters($request, $character_id);

        $total = $comics->count();
        $comics = $comics->skip($offset)->take($limit)->get();

        return ComicResourceCollection::make($comics)->init($offset, $limit, $total);
    }

    public function getCharacterEvents(EventsRequest $request, $character_id)
    {
        $offset = $request->get('offset') ? intval($request->get('offset')) : 0;
        $limit = $request->get('limit') ? intval($request->get('limit')) : 20;

        $events = Event::applyRelations()->applyFilters($request, $character_id);

        $total = $events->count();
        $events = $events->skip($offset)->take($limit)->get();

        return EventResourceCollection::make($events)->init($offset, $limit, $total);
    }

    public function getCharacterSeries(SeriesRequest $request, $character_id)
    {
        $offset = $request->get('offset') ? intval($request->get('offset')) : 0;
        $limit = $request->get('limit') ? intval($request->get('limit')) : 20;

        $series = Serie::applyRelations()->applyFilters($request, $character_id);

        $total = $series->count();
        $series = $series->skip($offset)->take($limit)->get();

        return SerieResourceCollection::make($series)->init($offset, $limit, $total);
    }

    public function getCreators(CreatorsRequest $request)
    {
        return Creator::get();
    }

    public function getStories(StoriesRequest $request)
    {
        return Story::get();
    }

    public function get3(Request $request)
    {
        $characters = Character::with(['comics' => function ($query) {
            $query->selectRaw('id, title');
        }])->withCount(['comics.creators'])->get()->toArray();

        $charactersIds = array_map(function ($element) {
            return $element['id'];
        }, $characters);

        $stories = DB::table('stories as s')
            ->join('comics_stories as cs', 's.id', '=', 'cs.story_id')
            ->join('comics as c', 'c.id', '=', 'cs.comic_id')
            ->join('comics_characters as cch', 'c.id', '=', 'cch.comic_id')
            ->whereIn('cch.character_id', $charactersIds)
            ->groupBy('s.id', 'cch.character_id')
            ->selectRaw('cch.character_id as character_id, s.id, s.title')
            ->limit(20)
            ->get();

        $series = DB::table('series as s')
            ->join('comics as c', 'c.serie_id', '=', 's.id')
            ->join('comics_characters as cch', 'c.id', '=', 'cch.comic_id')
            ->whereIn('cch.character_id', $charactersIds)
            ->groupBy('s.id', 'cch.character_id')
            ->selectRaw('cch.character_id as character_id, s.id, s.title');

        $series = [
            'available' => $series->count(),
            'items' => $series->limit(20)->get()
        ];

        $events = DB::table('events as e')
            ->join('comics_events as cs', 'e.id', '=', 'cs.event_id')
            ->join('comics as c', 'c.id', '=', 'cs.comic_id')
            ->join('comics_characters as cch', 'c.id', '=', 'cch.comic_id')
            ->whereIn('cch.character_id', $charactersIds)
            ->groupBy('e.id', 'cch.character_id')
            ->selectRaw('cch.character_id as character_id, e.id, e.title');

        $events = [
            'available' => $events->count(),
            'items' => $events->limit(20)->get()
        ];

        return $series;
    }

    public function get2(Request $request)
    {
        $characters =
            Character::get()->toArray();

        // $charactersIds = array_map(function ($element) {
        //     return $element['id'];
        // }, $characters);

        foreach ($characters as &$c) {
            $comics = Comic::whereHas('characters', function ($query) use ($c) {
                $query->where('character_id', $c['id']);
            })->select(['id', 'title']);

            $c['comics'] = [
                'available' => $comics->count(),
                'items' => $comics->limit(20)->get()
            ];
            $c['comics']['returned'] = sizeof($c['comics']['items']);

            // $creators = Creator::whereHas('comics.characters', function ($query) use ($c) {
            //     $query->where('character_id', $c['id']);
            // });

            // $c['creators'] = [
            //     'available' => $creators->count(),
            //     'items' => $creators->limit(20)->get()
            // ];
            // $c['creators']['returned'] = sizeof($c['creators']['items']);
        }

        // $characters['comics'] = Comic::whereHas('characters', function ($query) use ($charactersIds) {
        //     $query->whereIn('character_id', $charactersIds);
        // })->with(['characters'])->limit(20)->get();

        // $characters['creators'] = Creator::whereHas('comics.characters', function ($query) use ($charactersIds) {
        //     $query->whereIn('character_id', $charactersIds);
        // })->limit(20)->get();

        return $characters;
    }
}
