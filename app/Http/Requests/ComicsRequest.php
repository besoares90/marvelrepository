<?php

namespace App\Http\Requests;

use App\Enums\Enums\ComicsParametersEnum;
use App\Enums\Enums\EntityListEnum;
use App\Enums\Enums\EntityListRuleEnum;
use App\Rules\DateDescriptorRule;
use App\Rules\DateRangeRule;
use App\Rules\IdListRule;
use App\Rules\LimitRule;
use App\Rules\OffsetRule;
use App\Rules\OrderByRule;
use App\Rules\StartYearRule;
use Illuminate\Http\Exceptions\HttpResponseException;

class ComicsRequest extends MarvelRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $params = $this->request->all();

        if (sizeof($params) > 0){
            $validParameters = ComicsParametersEnum::getValues();

            foreach ($params as $param => $value) {
                if (!in_array($param, $validParameters)){
                    throw new HttpResponseException(response()->json([
                        'code' => 409,
                        'status' => "We don't recognize the parameter ".$param
                    ], 409));
                }
            }
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'format' => ['sometimes','required'],
            'formatType' => ['sometimes','required'],
            'noVariants' => ['sometimes','required'],
            'dateDescriptor' => ['sometimes','required', new DateDescriptorRule], //caso nao retorne dentro de um dos valores possíveis, retorna 500
            'dateRange' => ['sometimes','required', new DateRangeRule],
            /*date range, a ordem importa: ex: 
                2020-08-22,2020-08-01 nao retorna nada, mas
                2020-08-01,2020-08-22 sim (127 registros)

                caso a primeira data seja uma data válida, ex: 2020-08-01
                ignora a segunda data nao (ex: 2020), e retorna até o dia atual,
                caso a primeira data seja invalida e a segunda data seja valida, nao retorna nada
            */
            'title' => 'sometimes|required|bail',
            'titleStartsWith' => 'sometimes|required|bail',
            'startYear' => ['sometimes', 'required', new StartYearRule],
            'issueNumber' => ['sometimes', 'required', 'integer'],
            'diamondCode' => 'sometimes|required|bail',
            //'digitalId' => 'sometimes|required|bail', //no validation identified
            'upc' => 'sometimes|required|bail',
            'isbn' => 'sometimes|required|bail',
            'ean' => 'sometimes|required|bail',
            'issn' => 'sometimes|required|bail',
            //'hasDigitalIssue' => 'sometimes|required|bail', //no validation identified
            //'modifiedSince' => 'sometimes|required|bail', //no validation identified
            'creators' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::CREATOR)],
            'characters' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::CHARACTER)],
            'series' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::SERIES)],
            'events' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::EVENT)],
            'stories' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::STORY)],
            'sharedAppearances' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::SHAREDAPPEARANCES)],
            'collaborators' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::COLLABORATORS)],
            'orderBy' => ['sometimes', 'nullable', new OrderByRule(EntityListEnum::COMIC)],
            'limit' => ['sometimes', new LimitRule],
            'offset' => ['sometimes', new OffsetRule], //force error 500 if offset < 0
        ];
    }
}
