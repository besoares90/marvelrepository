<?php

namespace App\Http\Requests;

use App\Enums\Enums\CreatorsParametersEnum;
use App\Enums\Enums\EntityListEnum;
use App\Enums\Enums\EntityListRuleEnum;
use App\Rules\IdListRule;
use App\Rules\LimitRule;
use App\Rules\OffsetRule;
use App\Rules\OrderByRule;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreatorsRequest extends MarvelRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $params = $this->request->all();

        if (sizeof($params) > 0){
            $validParameters = CreatorsParametersEnum::getValues();

            foreach ($params as $param => $value) {
                if (!in_array($param, $validParameters)){
                    throw new HttpResponseException(response()->json([
                        'code' => 409,
                        'status' => "We don't recognize the parameter ".$param
                    ], 409));
                }
            }
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'sometimes|required|bail',
            'middleName' => 'sometimes|required|bail',
            'lastName' => 'sometimes|required|bail',
            'suffix' => 'sometimes|required|bail',
            'nameStartsWith' => 'sometimes|required|bail',
            'firstNameStartsWith' => 'sometimes|required|bail',
            'middleNameStartsWith' => 'sometimes|required|bail',
            'lastNameStartsWith' => 'sometimes|required|bail',
            //'modifiedSince' => 'sometimes|required|bail', //no validation identified
            'comics' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::ISSUE)],
            'series' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::SERIES)],
            'events' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::EVENT)],
            'stories' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::STORY)],
            'orderBy' => ['sometimes', 'nullable', new OrderByRule(EntityListEnum::CREATOR)],
            'limit' => ['sometimes', new LimitRule],
            'offset' => ['sometimes', new OffsetRule], //force error 500 if offset < 0
        ];
    }
}
