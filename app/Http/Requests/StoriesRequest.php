<?php

namespace App\Http\Requests;

use App\Enums\Enums\EntityListEnum;
use App\Enums\Enums\EntityListRuleEnum;
use App\Enums\Enums\StoriesParametersEnum;
use App\Rules\IdListRule;
use App\Rules\LimitRule;
use App\Rules\OffsetRule;
use App\Rules\OrderByRule;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoriesRequest extends MarvelRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $params = $this->request->all();

        if (sizeof($params) > 0){
            $validParameters = StoriesParametersEnum::getValues();

            foreach ($params as $param => $value) {
                if (!in_array($param, $validParameters)){
                    throw new HttpResponseException(response()->json([
                        'code' => 409,
                        'status' => "We don't recognize the parameter ".$param
                    ], 409));
                }
            }
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'modifiedSince' => 'sometimes|required|bail', //no validation identified
            'comics' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::ISSUE)],
            'series' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::SERIES)],
            'events' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::EVENT)],
            'creators' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::CREATOR)],
            'characters' => ['sometimes', 'required', new IdListRule(EntityListRuleEnum::CHARACTER)],

            'orderBy' => ['sometimes', 'nullable', new OrderByRule(EntityListEnum::STORY)],
            'limit' => ['sometimes', new LimitRule],
            'offset' => ['sometimes', new OffsetRule], //force error 500 if offset < 0
        ];
    }
}
