<?php

namespace App\Http\Resources;

use App\Enums\Enums\EntityListEnum;
use App\Http\Resources\ChildCollections\ArrayResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class CharacterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'modified' => $this->modified_at,
            'thumbnail' => ThumbnailResource::make($this),
            'resourceURI' => config('app.api_url') . '/characters/' . $this->id,
            'comics' => ArrayResourceCollection::make($this->comics)->fill(EntityListEnum::COMIC_PLURAL, $this->comics_count, 'characters', $this->id),
            'series' => ArrayResourceCollection::make($this->series)->fill(EntityListEnum::SERIES_PLURAL, $this->series_count, 'characters', $this->id),
            'stories' => ArrayResourceCollection::make($this->stories)->fill(EntityListEnum::STORY_PLURAL, $this->stories_count, 'characters', $this->id),
            'events' => ArrayResourceCollection::make($this->events)->fill(EntityListEnum::EVENT_PLURAL, $this->events_count, 'characters', $this->id),
            'urls' => $this->urls
        ];
    }
}
