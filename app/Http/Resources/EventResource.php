<?php

namespace App\Http\Resources;

use App\Enums\Enums\EntityListEnum;
use App\Http\Resources\ChildCollections\ArrayResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'resourceURI' => config('app.api_url') . '/events/' . $this->id,
            'urls' => $this->urls,
            'modified' => $this->modified_at,
            'start' => $this->start,
            'end' => $this->end,
            'thumbnail' => ThumbnailResource::make($this),
            'creators' => ArrayResourceCollection::make($this->creators)->fill(EntityListEnum::CREATOR_PLURAL, $this->creators_count, 'event', $this->id),
            'characters' => ArrayResourceCollection::make($this->characters)->fill(EntityListEnum::CHARACTER_PLURAL, $this->characters_count, 'event', $this->id),
            'stories' => ArrayResourceCollection::make($this->stories)->fill(EntityListEnum::STORY_PLURAL, $this->stories_count, 'event', $this->id),
            'comics' => ArrayResourceCollection::make($this->comics)->fill(EntityListEnum::COMIC_PLURAL, $this->comics_count, 'event', $this->id),
            'series' => ArrayResourceCollection::make($this->series)->fill(EntityListEnum::SERIES_PLURAL, $this->series_count, 'event', $this->id),
            'next' => EventSimpleResource::make($this->next),
            'previous' => EventSimpleResource::make($this->previous),
        ];
    }
}
