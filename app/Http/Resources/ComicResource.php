<?php

namespace App\Http\Resources;

use App\Enums\Enums\EntityListEnum;
use App\Http\Resources\ChildCollections\ArrayResourceCollection;
use App\Http\Resources\ChildCollections\ComicSimpleArrayResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class ComicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'digitalId' => $this->digital_id,
            'title' => $this->title,
            'issueNumber' => $this->issue_number,
            'variantDescription' => $this->variant_description,
            'description' => $this->description,
            'modified' => $this->modified_at,
            'isbn' => $this->isbn,
            'upc' => $this->upc,
            'diamondCode' => $this->diamond_code ? $this->diamond_code : '',
            'ean' => $this->ean,
            'issn' => $this->issn,
            'format' => $this->format,
            'pageCount' => $this->page_count,
            'textObjects' => $this->text_objects,
            'resourceURI' => config('app.api_url') . '/comics/' . $this->id,
            'urls' => $this->urls,
            'series' => $this->serie ? [
                'resourceURI' => config('app.api_url') . '/series/' . $this->serie->id,
                'name' => $this->serie->title,
            ] : [],
            'variants' => ComicSimpleArrayResourceCollection::make($this->variants),
            'collections' => ComicSimpleArrayResourceCollection::make($this->collections),
            'collectedIssues' => ComicSimpleArrayResourceCollection::make($this->collected_issues),
            'dates' => $this->dates,
            'prices' => $this->prices,
            'thumbnail' => ThumbnailResource::make($this),
            'images' => $this->images,
            'creators' => ArrayResourceCollection::make($this->creators)->fill(EntityListEnum::CREATOR_PLURAL, $this->creators_count, 'comics', $this->id),
            'characters' => ArrayResourceCollection::make($this->characters)->fill(EntityListEnum::CHARACTER_PLURAL, $this->characters_count, 'comics', $this->id),
            'stories' => ArrayResourceCollection::make($this->stories)->fill(EntityListEnum::STORY_PLURAL, $this->stories_count, 'comics', $this->id),
            'events' => ArrayResourceCollection::make($this->events)->fill(EntityListEnum::EVENT_PLURAL, $this->events_count, 'comics', $this->id),
        ];
    }
}
