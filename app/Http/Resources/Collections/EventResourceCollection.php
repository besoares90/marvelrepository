<?php

namespace App\Http\Resources\Collections;

use App\Http\Resources\EventResource;

class EventResourceCollection extends MarvelResourceCollection
{
    public function init($offset, $limit, $total)
    {
        $this->offset = $offset;
        $this->limit = $limit;
        $this->total = $total;

        return $this;
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'offset' => $this->offset,
                'limit' => $this->limit,
                'total' => $this->total,
                'count' => sizeof($this->collection),
                'results' => EventResource::collection($this->collection)
            ]
        ];
    }    
}
