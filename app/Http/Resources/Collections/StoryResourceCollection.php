<?php

namespace App\Http\Resources\Collections;

use App\Http\Resources\CharacterResource;
use App\Http\Resources\StoryResource;

class StoryResourceCollection extends MarvelResourceCollection
{
    public function init($offset, $limit, $total)
    {
        $this->offset = $offset;
        $this->limit = $limit;
        $this->total = $total;

        return $this;
    }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'offset' => $this->offset,
                'limit' => $this->limit,
                'total' => $this->total,
                'count' => sizeof($this->collection),
                'results' => StoryResource::collection($this->collection)
            ]
        ];
    }    
}
