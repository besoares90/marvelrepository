<?php

namespace App\Http\Resources\Collections;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MarvelResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function withResponse($request, $response)
    {
        $data = (array) $response->getData();

        $response->setData(
            array_merge(
                [
                    'code' => 200,
                    'status' => 'Ok',
                    'copyright' => '© 2020 MARVEL',
                    'attributionText' => 'Data provided by Marvel. © 2020 MARVEL',
                    'attributionHTML' => '<a href=\"http://marvel.com\">Data provided by Marvel. © 2020 MARVEL</a>',
                    'etag' => '75c1792e2828fb06bd29e2e7fe1b7aefb61a4722'
                ],
                $data
            )
        );

        return $response;
    }
}
