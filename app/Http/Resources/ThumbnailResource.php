<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ThumbnailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //$extensions = ['png', 'jpg', 'jpeg'];
        
        return [
            'path' => $this->thumbnail,
            'extension' => 'png'//$extensions[array_rand($extensions)]
        ];
    }
}
