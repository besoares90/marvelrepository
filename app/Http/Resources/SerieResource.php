<?php

namespace App\Http\Resources;

use App\Enums\Enums\EntityListEnum;
use App\Http\Resources\ChildCollections\ArrayResourceCollection;
use App\Models\Event;
use Illuminate\Http\Resources\Json\JsonResource;

class SerieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'resourceURI' => config('app.api_url') . '/series/' . $this->id,
            'urls' => $this->urls,
            'startYear' => $this->start_year,
            'endYear' => $this->end_year,
            'rating' => $this->rating,
            'type' => $this->type,
            'modified' => $this->modified_at,
            'thumbnail' => ThumbnailResource::make($this),
            'creators' => ArrayResourceCollection::make($this->creators)->fill(EntityListEnum::CREATOR_PLURAL, $this->creators_count, 'series', $this->id),
            'characters' => ArrayResourceCollection::make($this->characters)->fill(EntityListEnum::CHARACTER_PLURAL, $this->characters_count, 'series', $this->id),
            'stories' => ArrayResourceCollection::make($this->stories)->fill(EntityListEnum::STORY_PLURAL, $this->stories_count, 'series', $this->id),
            'comics' => ArrayResourceCollection::make($this->comics)->fill(EntityListEnum::COMIC_PLURAL, $this->comics_count, 'series', $this->id),
            'events' => ArrayResourceCollection::make($this->events)->fill(EntityListEnum::EVENT_PLURAL, $this->events_count, 'series', $this->id),
        ];
    }
}
