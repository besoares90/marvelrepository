<?php

namespace App\Http\Resources;

use App\Enums\Enums\EntityListEnum;
use App\Http\Resources\ChildCollections\ArrayResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class StoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'resourceURI' => config('app.api_url') . '/stories/' . $this->id,
            'type' => $this->type,
            'modified' => $this->modified_at,
            'thumbnail' => ThumbnailResource::make($this),
            'creators' => ArrayResourceCollection::make($this->creators)->fill(EntityListEnum::CREATOR_PLURAL, $this->creators_count, 'stories', $this->id),
            'characters' => ArrayResourceCollection::make($this->characters)->fill(EntityListEnum::CHARACTER_PLURAL, $this->characters_count, 'stories', $this->id),
            'series' => ArrayResourceCollection::make($this->series)->fill(EntityListEnum::SERIES_PLURAL, $this->series_count, 'stories', $this->id),
            'comics' => ArrayResourceCollection::make($this->comics)->fill(EntityListEnum::COMIC_PLURAL, $this->comics_count, 'stories', $this->id),
            'events' => ArrayResourceCollection::make($this->events)->fill(EntityListEnum::EVENT_PLURAL, $this->events_count, 'stories', $this->id),
        ];
    }
}
