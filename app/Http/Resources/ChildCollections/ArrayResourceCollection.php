<?php

namespace App\Http\Resources\ChildCollections;

use App\Enums\Enums\EntityListEnum;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ArrayResourceCollection extends ResourceCollection
{
    protected $entity;
    protected $available;
    protected $parentName;
    protected $parentId;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fill($entity, $available, $parentName, $parentId)
    {
        $this->entity = $entity;
        $this->available = $available;
        $this->parentName = $parentName;
        $this->parentId = $parentId;

        return $this;
    }

    public function toArray($request)
    {
        return [
            'available' => $this->available,
            'collectionURI' => config('app.api_url') . '/' . $this->parentName . '/' . $this->parentId . '/' . $this->entity,
            'items' => $this->map(function ($element) {
                return [
                    'resourceURI' => config('app.api_url') . '/' . $this->entity . '/' . $element->id,
                    'name' => $this->getTextAttribute($element),
                ];
            }),
            'returned' => sizeof($this)
        ];
    }

    private function getTextAttribute($element)
    {
        switch ($this->entity) {
            case EntityListEnum::CHARACTER_PLURAL:
                return $element->name;
            case EntityListEnum::COMIC_PLURAL:
            case EntityListEnum::COMIC_ISSUE_PLURAL:
            case EntityListEnum::EVENT_PLURAL:
            case EntityListEnum::SERIES_PLURAL:
            case EntityListEnum::STORY_PLURAL:
                return $element->title;
                return $element->title;
            case EntityListEnum::CREATOR_PLURAL:
                return $element->first_name . ($element->middle_name ? (' ' . $element->middle_name) : '') . ($element->last_name ? ' ' . ($element->last_name) : '');
        }
    }
}
