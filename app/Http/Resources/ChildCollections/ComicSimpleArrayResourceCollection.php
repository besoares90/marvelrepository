<?php

namespace App\Http\Resources\ChildCollections;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ComicSimpleArrayResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
            return $this->map(function ($el) {
                return [
                    'resourceURI' => config('app.api_url') . '/comics/'.$el->id,
                    'name' => $el->title,
                ];
            });
    }
}
