<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use TestSeeder;

class StoriesFeaturesTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate:reset');
        Artisan::call('migrate');

        $this->seed(TestSeeder::class);
    }

    public function testGetStoriesFromCharacter()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories');

        $response->assertStatus(200);

        $response->assertJson($this->getAllStories());
    }

    //Limit
    public function testGetStoriesWithLimit()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?limit=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetStoriesWithLimitZero()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/stories?limit=0');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    public function testGetStoriesWithLimitNegative()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/stories?limit=-1');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    public function testGetStoriesWithLimitOver100()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/stories?limit=101');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not request more than 100 items.',
        ));
    }

    public function testGetStoriesWithLimitNotNumeric()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/stories?limit=notNumeric');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    //Offset
    public function testGetStoriesWithOffset()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?offset=1');

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'offset' => 1,
                'total' => 2,
                'count' => 1,
                'results' => [
                    ['id' => 2]
                ]
            ]
        ]);
    }

    public function testGetStoriesWithOffsetNegative()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/stories?offset=-1');

        $response->assertStatus(500);

        $response->assertExactJson(array(
            'code' => 500,
            'status' => 'Internal Server Error',
        ));
    }

    public function testGetStoriesFilteredByModifiedSince()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?modifiedSince=2010-01-02');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByModifiedSinceWrongDate()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?modifiedSince=wrongDate');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    //Filter By comics
    public function testGetStoriesFilteredByComics()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?comics=2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2]
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByComicsTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?comics=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByComicsAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?comics=2,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByComicsMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/stories?comics=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 issue ids.',
        ));
    }

    //Filter By series
    public function testGetStoriesFilteredBySeries()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?series=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredBySeriesTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?series=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredBySeriesAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?series=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredBySeriesMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?series=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 series ids.',
        ));
    }



    //Filter By events
    public function testGetStoriesFilteredByEvents()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?events=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1]
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByEventsTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?events=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByEventsAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?events=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByEventsMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/stories?events=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 event ids.',
        ));
    }

    //Filter By creators
    public function testGetStoriesFilteredByCreators()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?creators=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByCreatorsTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?creators=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByCreatorsAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?creators=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByCreatorsMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?creators=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 creator ids.',
        ));
    }

    //Filter By characters
    public function testGetStoriesFilteredByCharacters()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?characters=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByCharactersTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?characters=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByCharactersAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?characters=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetStoriesFilteredByCharactersMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?characters=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 character ids.',
        ));
    }

    //order by
    public function testGetStoriesOrderByWrongTerm()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/stories?orderBy=wrongTerm');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'wrongTerm is not a valid ordering parameter.',
        ));
    }

    public function testGetStoriesOrderByModified()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?orderBy=modified');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetStoriesOrderByModifiedDesc()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/stories?orderBy=-modified');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 2],
                    ['id' => 1],
                ]
            ]
        ]);
    }


    private function getAllStories()
    {
        return array(
            'code' => 200,
            'status' => 'Ok',
            'copyright' => '© 2020 MARVEL',
            'attributionText' => 'Data provided by Marvel. © 2020 MARVEL',
            'attributionHTML' => '<a href=\\"http://marvel.com\\">Data provided by Marvel. © 2020 MARVEL</a>',
            'etag' => '75c1792e2828fb06bd29e2e7fe1b7aefb61a4722',
            'data' =>
            array(
                'offset' => 0,
                'limit' => 20,
                'total' => 2,
                'count' => 2,
                'results' =>
                array(
                    0 =>
                    array(
                        'id' => 1,
                        'title' => 'Story 1',
                        'description' => 'Description 1',
                        'resourceURI' => config('app.api_url') . '/stories/1',
                        'type' => 'cover',
                        'thumbnail' =>
                        array(
                            'path' => 'thumbnailtest1.com',
                            'extension' => 'png',
                        ),
                        'creators' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/stories/1/creators',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/1',
                                    'name' => 'Creator 1 Middlename 1 Lastname 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/2',
                                    'name' => 'Creator 2 Middlename 2 Lastname 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'characters' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/stories/1/characters',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/1',
                                    'name' => 'Character 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/2',
                                    'name' => 'Character 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'series' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/stories/1/series',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/series/1',
                                    'name' => 'Serie 1',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'comics' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/stories/1/comics',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/comics/1',
                                    'name' => 'Comic 1',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'events' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/stories/1/events',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/1',
                                    'name' => 'Event 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/2',
                                    'name' => 'Event 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                    ),
                    1 =>
                    array(
                        'id' => 2,
                        'title' => 'Story 2',
                        'description' => 'Description 2',
                        'resourceURI' => config('app.api_url') . '/stories/2',
                        'type' => 'story',
                        'thumbnail' =>
                        array(
                            'path' => 'thumbnailtest2.com',
                            'extension' => 'png',
                        ),
                        'creators' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/stories/2/creators',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/2',
                                    'name' => 'Creator 2 Middlename 2 Lastname 2',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/3',
                                    'name' => 'Creator 3 Middlename 3 Lastname 3',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'characters' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/stories/2/characters',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/2',
                                    'name' => 'Character 2',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/3',
                                    'name' => 'Character 3',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'series' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/stories/2/series',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/series/2',
                                    'name' => 'Serie 2',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'comics' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/stories/2/comics',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/comics/2',
                                    'name' => 'Comic 2',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'events' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/stories/2/events',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/2',
                                    'name' => 'Event 2',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/3',
                                    'name' => 'Event 3',
                                ),
                            ),
                            'returned' => 2,
                        ),
                    ),
                ),
            ),
        );
    }
}
