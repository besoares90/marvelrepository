<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use TestSeeder;

class ComicsFeaturesTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate:reset');
        Artisan::call('migrate');

        $this->seed(TestSeeder::class);
    }

    public function testGetComicsFromCharacter()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics');

        $response->assertStatus(200);

        $response->assertJson($this->getAllComics());
    }

    //Limit
    public function testGetComicsWithLimit()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?limit=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsWithLimitZero()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/comics?limit=0');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    public function testGetComicsWithLimitNegative()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/comics?limit=-1');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    public function testGetComicsWithLimitOver100()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/comics?limit=101');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not request more than 100 items.',
        ));
    }

    public function testGetComicsWithLimitNotNumeric()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/comics?limit=notNumeric');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    //Offset
    public function testGetComicsWithOffset()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?offset=1');

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'offset' => 1,
                'total' => 2,
                'count' => 1,
                'results' => [
                    ['id' => 2]
                ]
            ]
        ]);
    }

    public function testGetComicsWithOffsetNegative()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/comics?offset=-1');

        $response->assertStatus(500);

        $response->assertExactJson(array(
            'code' => 500,
            'status' => 'Internal Server Error',
        ));
    }

    public function testGetComicsFilteredByModifiedSinceWrongDate()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?modifiedSince=wrongDate');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByFormatComic()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?format=comic');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['format' => 'comic'],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByFormatHardCover()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?format=hardcover');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['format' => 'hardcover'],
                ]
            ]
        ]);
    }

    //formatType e noVariats não compreendi


    public function testGetComicsFilteredByDateRange()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?dateRange=2017-01-01,2017-01-03');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    [
                        'dates' => [
                            ['type' => 'onsaleDate']
                        ]
                    ],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByDateRangeWrong()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?dateRange=wrong');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass both a start date and end date in order to send a date range.',
        ));
    }

    public function testGetComicsFilteredByTitle()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?title=Comic 1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['title' => 'Comic 1'],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByTitleEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?title=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'title cannot be blank if it is set.',
        ));
    }

    public function testGetComicsFilteredByTitleStartsWith()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?titleStartsWith=c');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByTitleStartsWithEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?titleStartsWith=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'titleStartsWith cannot be blank if it is set.',
        ));
    }

    public function testGetComicsFilteredByStartYearEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?startYear=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass a four-digit number if you set the series year filter.',
        ));
    }

    public function testGetComicsFilteredByStartYearLetter()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?startYear=a');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass a four-digit number if you set the series year filter.',
        ));
    }

    public function testGetComicsFilteredByStartYearLessThanFourdigit()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?startYear=123');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass a four-digit number if you set the series year filter.',
        ));
    }

    public function testGetComicsFilteredByIssueNumberLetter()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?issueNumber=a');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass a valid numeric issue number if you set the issue number filter.',
        ));
    }

    public function testGetComicsFilteredByIssueNumber()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?issueNumber=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByDiamondCodeEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?diamondCode=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'diamond code cannot be blank if it is set.',
        ));
    }

    public function testGetComicsFilteredByDiamondCode()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?diamondCode=diamond1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByDigitalId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?digitalId=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByDigitalIdLetter()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?digitalId=a');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 0,
                'count' => 0,
                'results' => []
            ]
        ]);
    }

    public function testGetComicsFilteredByUpcEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?upc=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'upc cannot be blank if it is set.',
        ));
    }

    public function testGetComicsFilteredByUpc()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?upc=upc2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByIsbnEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?isbn=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'isbn cannot be blank if it is set.',
        ));
    }

    public function testGetComicsFilteredByIsbn()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?isbn=isbn2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByEanEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?ean=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'ean cannot be blank if it is set.',
        ));
    }

    public function testGetComicsFilteredByEan()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?ean=ean2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByIssnEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?issn=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'issn cannot be blank if it is set.',
        ));
    }

    public function testGetComicsFilteredByIssn()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?issn=issn1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByHasDigitalIssue()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?hasDigitalIssue=true');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2],
                ]
            ]
        ]);
    }

    //Filter By creators
    public function testGetComicsFilteredByCreators()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?creators=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByCreatorsTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?creators=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByCreatorsAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?creators=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByCreatorsMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?creators=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 creator ids.',
        ));
    }

    //Filter By characters
    public function testGetComicsFilteredByCharacters()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?characters=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByCharactersTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?characters=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByCharactersAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?characters=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByCharactersMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?characters=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 character ids.',
        ));
    }


    //Filter By series
    public function testGetComicsFilteredBySeries()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?series=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredBySeriesTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?series=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredBySeriesAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?series=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredBySeriesMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?series=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 series ids.',
        ));
    }

    //Filter By events
    public function testGetComicsFilteredByEvents()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?events=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1]
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByEventsTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?events=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByEventsAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?events=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByEventsMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/comics?events=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 event ids.',
        ));
    }

    //Filter By stories
    public function testGetComicsFilteredByStories()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?stories=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByStoriesTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?stories=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByStoriesAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?stories=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByStoriesMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?stories=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 story ids.',
        ));
    }

    public function testGetComicsFilteredBySharedAppearancesEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?sharedAppearances=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'shared appearances cannot be blank if it is set.',
        ));
    }

    public function testGetComicsFilteredBySharedAppearances()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?sharedAppearances=2,3');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsFilteredByCollaboratorsEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?collaborators=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'collaborators cannot be blank if it is set.',
        ));
    }

    public function testGetComicsFilteredByCollaborators()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?collaborators=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }



    //order by
    //focDate e onSale date vão ficar faltando..
    public function testGetComicsOrderByWrongTerm()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/comics?orderBy=wrongTerm');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'wrongTerm is not a valid ordering parameter.',
        ));
    }

    public function testGetComicsOrderByTitle()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/comics?orderBy=title');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['title' => 'Comic 1'],
					['title' => 'Comic 2'],
				]
			]
		]);
	}

	public function testGetComicsOrderByTitleDesc()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/comics?orderBy=-title');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['title' => 'Comic 2'],
					['title' => 'Comic 1'],
				]
			]
		]);
    }
    
    public function testGetComicsOrderByIssueNumber()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/comics?orderBy=issueNumber');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['issueNumber' => 1],
					['issueNumber' => 2],
				]
			]
		]);
    }
    
    public function testGetComicsOrderByIssueNumberDesc()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/comics?orderBy=-issueNumber');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['issueNumber' => 2],
					['issueNumber' => 1],
				]
			]
		]);
	}

    public function testGetComicsOrderByModified()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?orderBy=modified');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetComicsOrderByModifiedDesc()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/comics?orderBy=-modified');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1], //datas iguais
                    ['id' => 2],
                ]
            ]
        ]);
    }


    private function getAllComics()
    {
        return array(
            'code' => 200,
            'status' => 'Ok',
            'copyright' => '© 2020 MARVEL',
            'attributionText' => 'Data provided by Marvel. © 2020 MARVEL',
            'attributionHTML' => '<a href=\\"http://marvel.com\\">Data provided by Marvel. © 2020 MARVEL</a>',
            'etag' => '75c1792e2828fb06bd29e2e7fe1b7aefb61a4722',
            'data' =>
            array(
                'offset' => 0,
                'limit' => 20,
                'total' => 2,
                'count' => 2,
                'results' =>
                array(
                    0 =>
                    array(
                        'id' => 1,
                        'digitalId' => 0,
                        'title' => 'Comic 1',
                        'issueNumber' => 1,
                        'variantDescription' => '',
                        'description' => 'Description 1',
                        'isbn' => 'isbn1',
                        'upc' => 'upc1',
                        'diamondCode' => 'diamond1',
                        'ean' => 'ean1',
                        'issn' => 'issn1',
                        'format' => 'comic',
                        'pageCount' => 1,
                        'textObjects' =>
                        array(
                            0 =>
                            array(
                                'type' => 'solicit text',
                                'language' => 'de',
                                'text' => 'text 1',
                            ),
                        ),
                        'resourceURI' => config('app.api_url') . '/comics/1',
                        'urls' =>
                        array(
                            0 =>
                            array(
                                'type' => 'type 2',
                                'url' => 'url2.com',
                            ),
                        ),
                        'series' =>
                        array(
                            'resourceURI' => config('app.api_url') . '/series/1',
                            'name' => 'Serie 1',
                        ),
                        'variants' =>
                        array(),
                        'collections' =>
                        array(),
                        'collectedIssues' =>
                        array(),
                        'dates' =>
                        array(
                            0 =>
                            array(
                                'type' => 'onsaleDate',
                            ),
                            1 =>
                            array(
                                'type' => 'focDate',
                            ),
                        ),
                        'prices' =>
                        array(
                            0 =>
                            array(
                                'type' => 'printPrice',
                                'price' => 2.99,
                            ),
                        ),
                        'thumbnail' =>
                        array(
                            'path' => 'thumb1.com',
                            'extension' => 'png',
                        ),
                        'images' =>
                        array(
                            0 =>
                            array(
                                'path' => 'path1.com',
                            ),
                        ),
                        'creators' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/comics/1/creators',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/1',
                                    'name' => 'Creator 1 Middlename 1 Lastname 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/2',
                                    'name' => 'Creator 2 Middlename 2 Lastname 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'characters' =>
                        array(
                            'available' => NULL,
                            'collectionURI' => config('app.api_url') . '/comics/1/characters',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/1',
                                    'name' => 'Character 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/2',
                                    'name' => 'Character 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'stories' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/comics/1/stories',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/stories/1',
                                    'name' => 'Story 1',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'events' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/comics/1/events',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/1',
                                    'name' => 'Event 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/2',
                                    'name' => 'Event 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                    ),
                    1 =>
                    array(
                        'id' => 2,
                        'digitalId' => 1,
                        'title' => 'Comic 2',
                        'issueNumber' => 2,
                        'variantDescription' => 'Variant Description 1',
                        'description' => 'Description 2',
                        'isbn' => 'isbn2',
                        'upc' => 'upc2',
                        'diamondCode' => 'diamond2',
                        'ean' => 'ean2',
                        'issn' => 'issn2',
                        'format' => 'hardcover',
                        'pageCount' => 2,
                        'textObjects' =>
                        array(
                            0 =>
                            array(
                                'type' => 'shorter text',
                                'language' => 'en-us',
                                'text' => 'text 2',
                            ),
                        ),
                        'resourceURI' => config('app.api_url') . '/comics/2',
                        'urls' =>
                        array(
                            0 =>
                            array(
                                'type' => 'type 2',
                                'url' => 'url2.com',
                            ),
                        ),
                        'series' =>
                        array(
                            'resourceURI' => config('app.api_url') . '/series/2',
                            'name' => 'Serie 2',
                        ),
                        'variants' =>
                        array(
                            0 =>
                            array(
                                'resourceURI' => config('app.api_url') . '/comics/1',
                                'name' => 'Comic 1',
                            ),
                        ),
                        'collections' =>
                        array(
                            0 =>
                            array(
                                'resourceURI' => config('app.api_url') . '/comics/1',
                                'name' => 'Comic 1',
                            ),
                        ),
                        'collectedIssues' =>
                        array(
                            0 =>
                            array(
                                'resourceURI' => config('app.api_url') . '/comics/1',
                                'name' => 'Comic 1',
                            ),
                        ),
                        'dates' =>
                        array(
                            0 =>
                            array(
                                'type' => 'onsaleDate',
                            ),
                            1 =>
                            array(
                                'type' => 'focDate',
                            ),
                        ),
                        'prices' =>
                        array(
                            0 =>
                            array(
                                'type' => 'printPrice',
                                'price' => 5,
                            ),
                        ),
                        'thumbnail' =>
                        array(
                            'path' => 'thumb2.com',
                            'extension' => 'png',
                        ),
                        'images' =>
                        array(
                            0 =>
                            array(
                                'path' => 'path2.com',
                            ),
                        ),
                        'creators' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/comics/2/creators',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/2',
                                    'name' => 'Creator 2 Middlename 2 Lastname 2',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/3',
                                    'name' => 'Creator 3 Middlename 3 Lastname 3',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'characters' =>
                        array(
                            'available' => NULL,
                            'collectionURI' => config('app.api_url') . '/comics/2/characters',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/2',
                                    'name' => 'Character 2',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/3',
                                    'name' => 'Character 3',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'stories' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/comics/2/stories',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/stories/2',
                                    'name' => 'Story 2',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'events' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/comics/2/events',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/2',
                                    'name' => 'Event 2',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/3',
                                    'name' => 'Event 3',
                                ),
                            ),
                            'returned' => 2,
                        ),
                    ),
                ),
            ),
        );
    }
}
