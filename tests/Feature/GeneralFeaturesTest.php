<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use TestSeeder;

class GeneralFeaturesTest extends TestCase
{
    // use RefreshDatabase;
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate:reset');
        Artisan::call('migrate');

        $this->seed(TestSeeder::class);
    }

    public function testWrongUrlResponse()
    {
        $response = $this->get(config('app.api_url') . '/wrongUrl');

        $response->assertStatus(404);

        $response->assertExactJson(array(
            'code' => 'ResourceNotFound',
            'message' => '/api/v1/public/wrongUrl does not exist',
        ));
    }
}