<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use TestSeeder;

class EventsFeaturesTest extends TestCase
{
    // use RefreshDatabase;
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate:reset');
        Artisan::call('migrate');

        $this->seed(TestSeeder::class);
    }

    public function testGetEventsFromCharacter()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events');

        $response->assertStatus(200);

        $response->assertJson($this->getAllEvents());
    }

    // Limit
    public function testGetEventsWithLimit()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?limit=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetEventsWithLimitZero()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?limit=0');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    public function testGetEventsWithLimitNegative()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?limit=-1');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    public function testGetEventsWithLimitOver100()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?limit=101');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not request more than 100 items.',
        ));
    }

    public function testGetEventsWithLimitNotNumeric()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?limit=notNumeric');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    //Offset
    public function testGetEventsWithOffset()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?offset=1');

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'offset' => 1,
                'total' => 2,
                'count' => 1,
                'results' => [
                    ['id' => 2]
                ]
            ]
        ]);
    }

    public function testGetEventsWithOffsetNegative()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?offset=-1');

        $response->assertStatus(500);

        $response->assertExactJson(array(
            'code' => 500,
            'status' => 'Internal Server Error',
        ));
    }

    //Start Filters

    public function testGetEventsFilteredByName()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?name=Event 1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['title' => 'Event 1'],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByNameEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?name=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'name cannot be blank if it is set.',
        ));
    }

    public function testGetEventsFilteredByNameStartsWith()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?nameStartsWith=ev');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByNameStartsWithEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?nameStartsWith=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'nameStartsWith cannot be blank if it is set.',
        ));
    }

    public function testGetEventsFilteredByModifiedSince()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?modifiedSince=2020-01-02');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByModifiedSinceWrongDate()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?modifiedSince=wrongDate');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    //Filter By creators
    public function testGetEventsFilteredByCreators()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?creators=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2]
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByCreatorsTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?creators=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByCreatorsAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?creators=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByCreatorsMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?creators=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 creator ids.',
        ));
    }

    //Filter By characters
    public function testGetEventsFilteredByCharacters()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?characters=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2]
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByCharactersTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?characters=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByCharactersAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?characters=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByCharactersMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?characters=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 character ids.',
        ));
    }

    //Filter By comics
    public function testGetEventsFilteredByComics()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?comics=2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2]
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByComicsTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?comics=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByComicsAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?comics=2,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByComicsMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?comics=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 issue ids.',
        ));
    }

    //Filter By series
    public function testGetEventsFilteredBySeries()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?series=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredBySeriesTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?series=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredBySeriesAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?series=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredBySeriesMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?series=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 series ids.',
        ));
    }

    //Filter By stories
    public function testGetEventsFilteredByStories()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?stories=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByStoriesTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?stories=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByStoriesAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?stories=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsFilteredByStoriesMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters/2/events?stories=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 story ids.',
        ));
    }

    //order by
    public function testGetEventsOrderByWrongTerm()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?orderBy=wrongTerm');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'wrongTerm is not a valid ordering parameter.',
        ));
    }

    public function testGetEventsOrderByName()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?orderBy=name');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['title' => 'Event 1'],
                    ['title' => 'Event 2'],
                ]
            ]
        ]);
    }

    public function testGetEventsOrderByNameDesc()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?orderBy=-name');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['title' => 'Event 2'],
                    ['title' => 'Event 1'],
                ]
            ]
        ]);
    }

    public function testGetEventsOrderByStartDate()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?orderBy=startDate');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsOrderByStartDateDesc()
    {
        $response = $this->get(config('app.api_url') . '/characters/1/events?orderBy=-startDate');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 2],
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetEventsOrderByModified()
    {
        $response = $this->get(config('app.api_url') . '/characters/3/events?orderBy=modified');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 3],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetEventsOrderByModifiedDesc()
    {
        $response = $this->get(config('app.api_url') . '/characters/3/events?orderBy=-modified');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }



    private function getAllEvents()
    {
        return array(
            'code' => 200,
            'status' => 'Ok',
            'copyright' => '© 2020 MARVEL',
            'attributionText' => 'Data provided by Marvel. © 2020 MARVEL',
            'attributionHTML' => '<a href=\\"http://marvel.com\\">Data provided by Marvel. © 2020 MARVEL</a>',
            'etag' => '75c1792e2828fb06bd29e2e7fe1b7aefb61a4722',
            'data' =>
            array(
                'offset' => 0,
                'limit' => 20,
                'total' => 2,
                'count' => 2,
                'results' =>
                array(
                    0 =>
                    array(
                        'id' => 1,
                        'title' => 'Event 1',
                        'description' => 'Description 1',
                        'resourceURI' => config('app.api_url') . '/events/1',
                        'urls' =>
                        array(),
                        'thumbnail' =>
                        array(
                            'path' => 'thumbnailtest1.com',
                            'extension' => 'png',
                        ),
                        'creators' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/event/1/creators',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/1',
                                    'name' => 'Creator 1 Middlename 1 Lastname 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/2',
                                    'name' => 'Creator 2 Middlename 2 Lastname 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'characters' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/event/1/characters',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/1',
                                    'name' => 'Character 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/2',
                                    'name' => 'Character 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'stories' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/event/1/stories',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/stories/1',
                                    'name' => 'Story 1',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'comics' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/event/1/comics',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/comics/1',
                                    'name' => 'Comic 1',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'series' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/event/1/series',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/series/1',
                                    'name' => 'Serie 1',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'next' =>
                        array(
                            'name' => 'Event 2',
                            'resourceURI' => config('app.api_url') . '/events/2',
                        ),
                        'previous' => NULL,
                    ),
                    1 =>
                    array(
                        'id' => 2,
                        'title' => 'Event 2',
                        'description' => 'Description 2',
                        'resourceURI' => config('app.api_url') . '/events/2',
                        'urls' =>
                        array(),
                        'thumbnail' =>
                        array(
                            'path' => 'thumbnailtest1.com',
                            'extension' => 'png',
                        ),
                        'creators' =>
                        array(
                            'available' => 3,
                            'collectionURI' => config('app.api_url') . '/event/2/creators',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/1',
                                    'name' => 'Creator 1 Middlename 1 Lastname 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/2',
                                    'name' => 'Creator 2 Middlename 2 Lastname 2',
                                ),
                                2 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/creators/3',
                                    'name' => 'Creator 3 Middlename 3 Lastname 3',
                                ),
                            ),
                            'returned' => 3,
                        ),
                        'characters' =>
                        array(
                            'available' => 3,
                            'collectionURI' => config('app.api_url') . '/event/2/characters',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/1',
                                    'name' => 'Character 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/2',
                                    'name' => 'Character 2',
                                ),
                                2 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/characters/3',
                                    'name' => 'Character 3',
                                ),
                            ),
                            'returned' => 3,
                        ),
                        'stories' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/event/2/stories',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/stories/1',
                                    'name' => 'Story 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/stories/2',
                                    'name' => 'Story 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'comics' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/event/2/comics',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/comics/1',
                                    'name' => 'Comic 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/comics/2',
                                    'name' => 'Comic 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'series' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/event/2/series',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/series/1',
                                    'name' => 'Serie 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/series/2',
                                    'name' => 'Serie 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'next' =>
                        array(
                            'name' => 'Event 3',
                            'resourceURI' => config('app.api_url') . '/events/3',
                        ),
                        'previous' =>
                        array(
                            'name' => 'Event 1',
                            'resourceURI' => config('app.api_url') . '/events/1',
                        ),
                    ),
                ),
            ),
        );
    }
}
