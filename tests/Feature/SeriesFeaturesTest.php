<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use TestSeeder;

class SeriesFeaturesTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp(): void
	{
		parent::setUp();

		Artisan::call('migrate:reset');
		Artisan::call('migrate');

		$this->seed(TestSeeder::class);
	}

	public function testGetSeriesFromCharacter()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series');

		$response->assertStatus(200);

		$response->assertJson($this->getAllSeries());
	}

	//Limit
	public function testGetSeriesWithLimit()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?limit=1');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 1,
				'results' => [
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesWithLimitZero()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?limit=0');

		$response->assertStatus(409);

		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'You must pass an integer limit greater than 0.',
		));
	}

	public function testGetSeriesWithLimitNegative()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?limit=-1');

		$response->assertStatus(409);

		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'You must pass an integer limit greater than 0.',
		));
	}

	public function testGetSeriesWithLimitOver100()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?limit=101');

		$response->assertStatus(409);

		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'You may not request more than 100 items.',
		));
	}

	public function testGetSeriesWithLimitNotNumeric()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?limit=notNumeric');

		$response->assertStatus(409);

		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'You must pass an integer limit greater than 0.',
		));
	}

	//Offset
	public function testGetSeriesWithOffset()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?offset=1');

		$response->assertStatus(200);

		$response->assertJson([
			'data' => [
				'offset' => 1,
				'total' => 2,
				'count' => 1,
				'results' => [
					['id' => 2]
				]
			]
		]);
	}

	public function testGetSeriesWithOffsetNegative()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?offset=-1');

		$response->assertStatus(500);

		$response->assertExactJson(array(
			'code' => 500,
			'status' => 'Internal Server Error',
		));
	}

	//Start Filters
	public function testGetSeriesFilteredByTitle()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?title=Serie 1');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['title' => 'Serie 1'],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByTitleEmpty()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?title=');

		$response->assertStatus(409);
		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'title cannot be blank if it is set.',
		));
	}

	public function testGetSeriesFilteredByTitleStartsWith()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?titleStartsWith=ser');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByTitleStartsWithEmpty()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?titleStartsWith=');

		$response->assertStatus(409);
		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'titleStartsWith cannot be blank if it is set.',
		));
	}

	public function testGetSeriesFilteredByModifiedSince()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?modifiedSince=2010-01-02');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByModifiedSinceWrongDate()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?modifiedSince=wrongDate');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	//Filter By comics
	public function testGetSeriesFilteredByComics()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?comics=2');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 2]
				]
			]
		]);
	}

	public function testGetSeriesFilteredByComicsTwoIds()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?comics=1,2');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByComicsAtLeastOneValidId()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?comics=2,a,b');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByComicsMoreThanTenTerms()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?comics=1,2,3,4,5,6,7,8,9,10,11');

		$response->assertStatus(409);
		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'You may not submit more than 10 issue ids.',
		));
	}

	//Filter By stories
	public function testGetSeriesFilteredByStories()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?stories=1');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByStoriesTwoIds()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?stories=1,2');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByStoriesAtLeastOneValidId()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?stories=1,a,b');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByStoriesMoreThanTenTerms()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?stories=1,2,3,4,5,6,7,8,9,10,11');

		$response->assertStatus(409);
		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'You may not submit more than 10 story ids.',
		));
	}

	//Filter By events
	public function testGetSeriesFilteredByEvents()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?events=1');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 1]
				]
			]
		]);
	}

	public function testGetSeriesFilteredByEventsTwoIds()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?events=1,2');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByEventsAtLeastOneValidId()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?events=2,a,b');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredBySeriesMoreThanTenTerms()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?events=1,2,3,4,5,6,7,8,9,10,11');

		$response->assertStatus(409);
		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'You may not submit more than 10 event ids.',
		));
	}

	//Filter By creators
	public function testGetSeriesFilteredByCreators()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?creators=1');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByCreatorsTwoIds()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?creators=1,2');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByCreatorsAtLeastOneValidId()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?creators=1,a,b');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByCreatorsMoreThanTenTerms()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?creators=1,2,3,4,5,6,7,8,9,10,11');

		$response->assertStatus(409);
		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'You may not submit more than 10 creator ids.',
		));
	}

	//Filter By characters
	public function testGetSeriesFilteredByCharacters()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?characters=1');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByCharactersTwoIds()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?characters=1,2');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByCharactersAtLeastOneValidId()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?characters=1,a,b');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByCharactersMoreThanTenTerms()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?characters=1,2,3,4,5,6,7,8,9,10,11');

		$response->assertStatus(409);
		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'You may not submit more than 10 character ids.',
		));
	}

	//Other filters
	public function testGetSeriesFilteredBySeriesTypeWrongType()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?seriesType=wrongType');

		$response->assertStatus(409);
		$response->assertJson(array(
			'code' => 409,
			'status' => 'You must pass at least one valid type if you set the seriesType filter.',
		));
	}

	public function testGetSeriesFilteredBySeriesTypeCollection()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?seriesType=collection');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesFilteredBySeriesTypeOnGoing()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?seriesType=ongoing');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByContainsComic()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?contains=comic');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByContainsHardCover()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?contains=hardcover');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 1,
				'count' => 1,
				'results' => [
					['id' => 2],
				]
			]
		]);
	}

	//order by
	public function testGetSeriesOrderByWrongTerm()
	{
		$response = $this->get(config('app.api_url') . '/characters/1/series?orderBy=wrongTerm');

		$response->assertStatus(409);
		$response->assertExactJson(array(
			'code' => 409,
			'status' => 'wrongTerm is not a valid ordering parameter.',
		));
	}

	public function testGetSeriesOrderByTitle()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?orderBy=title');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['title' => 'Serie 1'],
					['title' => 'Serie 2'],
				]
			]
		]);
	}

	public function testGetSeriesOrderByTitleDesc()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?orderBy=-title');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['title' => 'Serie 2'],
					['title' => 'Serie 1'],
				]
			]
		]);
	}

	public function testGetSeriesOrderByStartYear()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?orderBy=startYear');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesOrderByStartYearDesc()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?orderBy=-startYear');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 2],
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesOrderByModified()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?orderBy=modified');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesOrderByModifiedDesc()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?orderBy=-modified');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 2],
					['id' => 1],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByContainsTwoValues()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?contains=comic,hardcover');

		$response->assertStatus(200);
		$response->assertJson([
			'data' => [
				'offset' => 0,
				'total' => 2,
				'count' => 2,
				'results' => [
					['id' => 1],
					['id' => 2],
				]
			]
		]);
	}

	public function testGetSeriesFilteredByContainsWrongValue()
	{
		$response = $this->get(config('app.api_url') . '/characters/2/series?contains=wrong');

		$response->assertStatus(409);
		$response->assertJson(array(
			'code' => 409,
			'status' => 'You must pass at least one valid type if you set the seriesType filter.',
		));
	}


	private function getAllSeries()
	{
		return array(
			'code' => 200,
			'status' => 'Ok',
			'copyright' => '© 2020 MARVEL',
			'attributionText' => 'Data provided by Marvel. © 2020 MARVEL',
			'attributionHTML' => '<a href=\\"http://marvel.com\\">Data provided by Marvel. © 2020 MARVEL</a>',
			'etag' => '75c1792e2828fb06bd29e2e7fe1b7aefb61a4722',
			'data' =>
			array(
				'offset' => 0,
				'limit' => 20,
				'total' => 2,
				'count' => 2,
				'results' =>
				array(
					0 =>
					array(
						'id' => 1,
						'title' => 'Serie 1',
						'description' => 'Description 1',
						'resourceURI' => config('app.api_url') . '/series/1',
						'urls' =>
						array(),
						'startYear' => 2010,
						'endYear' => 2020,
						'rating' => 'Rated T',
						'type' => 'collection',
						'thumbnail' =>
						array(
							'path' => 'thumbnailtest.com',
							'extension' => 'png',
						),
						'creators' =>
						array(
							'available' => 2,
							'collectionURI' => config('app.api_url') . '/series/1/creators',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/creators/1',
									'name' => 'Creator 1 Middlename 1 Lastname 1',
								),
								1 =>
								array(
									'resourceURI' => config('app.api_url') . '/creators/2',
									'name' => 'Creator 2 Middlename 2 Lastname 2',
								),
							),
							'returned' => 2,
						),
						'characters' =>
						array(
							'available' => 2,
							'collectionURI' => config('app.api_url') . '/series/1/characters',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/characters/1',
									'name' => 'Character 1',
								),
								1 =>
								array(
									'resourceURI' => config('app.api_url') . '/characters/2',
									'name' => 'Character 2',
								),
							),
							'returned' => 2,
						),
						'stories' =>
						array(
							'available' => 1,
							'collectionURI' => config('app.api_url') . '/series/1/stories',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/stories/1',
									'name' => 'Story 1',
								),
							),
							'returned' => 1,
						),
						'comics' =>
						array(
							'available' => 1,
							'collectionURI' => config('app.api_url') . '/series/1/comics',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/comics/1',
									'name' => 'Comic 1',
								),
							),
							'returned' => 1,
						),
						'events' =>
						array(
							'available' => 2,
							'collectionURI' => config('app.api_url') . '/series/1/events',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/events/1',
									'name' => 'Event 1',
								),
								1 =>
								array(
									'resourceURI' => config('app.api_url') . '/events/2',
									'name' => 'Event 2',
								),
							),
							'returned' => 2,
						),
					),
					1 =>
					array(
						'id' => 2,
						'title' => 'Serie 2',
						'description' => 'Description 2',
						'resourceURI' => config('app.api_url') . '/series/2',
						'urls' =>
						array(),
						'startYear' => 2011,
						'endYear' => 2021,
						'rating' => 'Rated T+',
						'type' => 'ongoing',
						'thumbnail' =>
						array(
							'path' => 'thumbnailtest2.com',
							'extension' => 'png',
						),
						'creators' =>
						array(
							'available' => 2,
							'collectionURI' => config('app.api_url') . '/series/2/creators',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/creators/2',
									'name' => 'Creator 2 Middlename 2 Lastname 2',
								),
								1 =>
								array(
									'resourceURI' => config('app.api_url') . '/creators/3',
									'name' => 'Creator 3 Middlename 3 Lastname 3',
								),
							),
							'returned' => 2,
						),
						'characters' =>
						array(
							'available' => 2,
							'collectionURI' => config('app.api_url') . '/series/2/characters',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/characters/2',
									'name' => 'Character 2',
								),
								1 =>
								array(
									'resourceURI' => config('app.api_url') . '/characters/3',
									'name' => 'Character 3',
								),
							),
							'returned' => 2,
						),
						'stories' =>
						array(
							'available' => 1,
							'collectionURI' => config('app.api_url') . '/series/2/stories',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/stories/2',
									'name' => 'Story 2',
								),
							),
							'returned' => 1,
						),
						'comics' =>
						array(
							'available' => 1,
							'collectionURI' => config('app.api_url') . '/series/2/comics',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/comics/2',
									'name' => 'Comic 2',
								),
							),
							'returned' => 1,
						),
						'events' =>
						array(
							'available' => 2,
							'collectionURI' => config('app.api_url') . '/series/2/events',
							'items' =>
							array(
								0 =>
								array(
									'resourceURI' => config('app.api_url') . '/events/2',
									'name' => 'Event 2',
								),
								1 =>
								array(
									'resourceURI' => config('app.api_url') . '/events/3',
									'name' => 'Event 3',
								),
							),
							'returned' => 2,
						),
					),
				),
			),
		);
	}
}
