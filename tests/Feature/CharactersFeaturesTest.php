<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use TestSeeder;

class CharactersFeaturesTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate:reset');
        Artisan::call('migrate');

        $this->seed(TestSeeder::class);
    }

    public function testGetCharacters()
    {
        $response = $this->get(config('app.api_url') . '/characters');

        $response->assertStatus(200);

        $response->assertJson($this->getAllCharacters());
    }

    // Limit
    public function testGetCharactersWithLimit()
    {
        $response = $this->get(config('app.api_url') . '/characters?limit=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 1,
                'results' => [
                    ['id' => 1],
                ]
            ]
        ]);
    }

    public function testGetCharactersWithLimitZero()
    {
        $response = $this->get(config('app.api_url') . '/characters?limit=0');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    public function testGetCharactersWithLimitNegative()
    {
        $response = $this->get(config('app.api_url') . '/characters?limit=-1');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    public function testGetCharactersWithLimitOver100()
    {
        $response = $this->get(config('app.api_url') . '/characters?limit=101');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not request more than 100 items.',
        ));
    }

    public function testGetCharactersWithLimitNotNumeric()
    {
        $response = $this->get(config('app.api_url') . '/characters?limit=notNumeric');

        $response->assertStatus(409);

        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You must pass an integer limit greater than 0.',
        ));
    }

    //Offset
    public function testGetCharactersWithOffset()
    {
        $response = $this->get(config('app.api_url') . '/characters?offset=1');

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'offset' => 1,
                'total' => 3,
                'count' => 2,
                'results' => [
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetCharactersWithOffsetNegative()
    {
        $response = $this->get(config('app.api_url') . '/characters?offset=-1');

        $response->assertStatus(500);

        $response->assertExactJson(array(
            'code' => 500,
            'status' => 'Internal Server Error',
        ));
    }

    //Start Filters

    public function testGetCharactersFilteredByName()
    {
        $response = $this->get(config('app.api_url') . '/characters?name=Character 1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['name' => 'Character 1'],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByNameEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters?name=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'name cannot be blank if it is set.',
        ));
    }

    public function testGetCharactersFilteredByNameStartsWith()
    {
        $response = $this->get(config('app.api_url') . '/characters?nameStartsWith=char');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByNameStartsWithEmpty()
    {
        $response = $this->get(config('app.api_url') . '/characters?nameStartsWith=');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'nameStartsWith cannot be blank if it is set.',
        ));
    }

    public function testGetCharactersFilteredByModifiedSince()
    {
        $response = $this->get(config('app.api_url') . '/characters?modifiedSince=2010-01-03');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 1,
                'count' => 1,
                'results' => [
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByModifiedSinceWrongDate()
    {
        $response = $this->get(config('app.api_url') . '/characters?modifiedSince=wrongDate');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    //Filter By comics
    public function testGetCharactersFilteredByComics()
    {
        $response = $this->get(config('app.api_url') . '/characters?comics=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByComicsTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters?comics=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByComicsAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters?comics=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByComicsMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters?comics=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 issue ids.',
        ));
    }

    //Filter By series
    public function testGetCharactersFilteredBySeries()
    {
        $response = $this->get(config('app.api_url') . '/characters?series=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredBySeriesTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters?series=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredBySeriesAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters?series=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredBySeriesMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters?series=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 series ids.',
        ));
    }

    //Filter By events
    public function testGetCharactersFilteredByEvents()
    {
        $response = $this->get(config('app.api_url') . '/characters?events=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByEventsTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters?events=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByEventsAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters?events=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByEventsMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters?events=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 event ids.',
        ));
    }

    //Filter By stories
    public function testGetCharactersFilteredByStories()
    {
        $response = $this->get(config('app.api_url') . '/characters?stories=1');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByStoriesTwoIds()
    {
        $response = $this->get(config('app.api_url') . '/characters?stories=1,2');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByStoriesAtLeastOneValidId()
    {
        $response = $this->get(config('app.api_url') . '/characters?stories=1,a,b');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 2,
                'count' => 2,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                ]
            ]
        ]);
    }

    public function testGetCharactersFilteredByStoriesMoreThanTenTerms()
    {
        $response = $this->get(config('app.api_url') . '/characters?stories=1,2,3,4,5,6,7,8,9,10,11');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'You may not submit more than 10 story ids.',
        ));
    }

    //order by
    public function testGetCharactersOrderByWrongTerm()
    {
        $response = $this->get(config('app.api_url') . '/characters?orderBy=wrongTerm');

        $response->assertStatus(409);
        $response->assertExactJson(array(
            'code' => 409,
            'status' => 'wrongTerm is not a valid ordering parameter.',
        ));
    }

    public function testGetCharactersOrderByName()
    {
        $response = $this->get(config('app.api_url') . '/characters?orderBy=name');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['name' => 'Character 1'],
                    ['name' => 'Character 2'],
                    ['name' => 'Character 3'],
                ]
            ]
        ]);
    }

    public function testGetCharactersOrderByNameDesc()
    {
        $response = $this->get(config('app.api_url') . '/characters?orderBy=-name');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['name' => 'Character 3'],
                    ['name' => 'Character 2'],
                    ['name' => 'Character 1'],
                ]
            ]
        ]);
    }

    public function testGetCharactersOrderByModified()
    {
        $response = $this->get(config('app.api_url') . '/characters?orderBy=modified');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 3],
                ]
            ]
        ]);
    }

    public function testGetCharactersOrderByModifiedDesc()
    {
        $response = $this->get(config('app.api_url') . '/characters?orderBy=-modified');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'offset' => 0,
                'total' => 3,
                'count' => 3,
                'results' => [
                    ['id' => 3],
                    ['id' => 2],
                    ['id' => 1],
                ]
            ]
        ]);
    }

    private function getAllCharacters()
    {
        return array(
            'code' => 200,
            'status' => 'Ok',
            'copyright' => '© 2020 MARVEL',
            'attributionText' => 'Data provided by Marvel. © 2020 MARVEL',
            'attributionHTML' => '<a href=\\"http://marvel.com\\">Data provided by Marvel. © 2020 MARVEL</a>',
            'etag' => '75c1792e2828fb06bd29e2e7fe1b7aefb61a4722',
            'data' =>
            array(
                'offset' => 0,
                'limit' => 20,
                'total' => 3,
                'count' => 3,
                'results' =>
                array(
                    0 =>
                    array(
                        'id' => 1,
                        'name' => 'Character 1',
                        'description' => 'Description 1',
                        'thumbnail' =>
                        array(
                            'path' => 'thumbnailtest1.com',
                            'extension' => 'png',
                        ),
                        'resourceURI' => config('app.api_url') . '/characters/1',
                        'comics' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/characters/1/comics',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/comics/1',
                                    'name' => 'Comic 1',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'series' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/characters/1/series',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/series/1',
                                    'name' => 'Serie 1',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'stories' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/characters/1/stories',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/stories/1',
                                    'name' => 'Story 1',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'events' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/characters/1/events',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/1',
                                    'name' => 'Event 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/2',
                                    'name' => 'Event 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'urls' =>
                        array(
                            0 =>
                            array(
                                'type' => 'type 1',
                                'url' => 'url1.com',
                            ),
                        ),
                    ),
                    1 =>
                    array(
                        'id' => 2,
                        'name' => 'Character 2',
                        'description' => 'Description 2',
                        'thumbnail' =>
                        array(
                            'path' => 'thumbnailtest2.com',
                            'extension' => 'png',
                        ),
                        'resourceURI' => config('app.api_url') . '/characters/2',
                        'comics' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/characters/2/comics',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/comics/1',
                                    'name' => 'Comic 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/comics/2',
                                    'name' => 'Comic 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'series' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/characters/2/series',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/series/1',
                                    'name' => 'Serie 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/series/2',
                                    'name' => 'Serie 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'stories' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/characters/2/stories',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/stories/1',
                                    'name' => 'Story 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/stories/2',
                                    'name' => 'Story 2',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'events' =>
                        array(
                            'available' => 3,
                            'collectionURI' => config('app.api_url') . '/characters/2/events',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/1',
                                    'name' => 'Event 1',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/2',
                                    'name' => 'Event 2',
                                ),
                                2 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/3',
                                    'name' => 'Event 3',
                                ),
                            ),
                            'returned' => 3,
                        ),
                        'urls' =>
                        array(
                            0 =>
                            array(
                                'type' => 'type 1',
                                'url' => 'url1.com',
                            ),
                        ),
                    ),
                    2 =>
                    array(
                        'id' => 3,
                        'name' => 'Character 3',
                        'description' => 'Description 3',
                        'thumbnail' =>
                        array(
                            'path' => 'thumbnailtest3.com',
                            'extension' => 'png',
                        ),
                        'resourceURI' => config('app.api_url') . '/characters/3',
                        'comics' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/characters/3/comics',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/comics/2',
                                    'name' => 'Comic 2',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'series' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/characters/3/series',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/series/2',
                                    'name' => 'Serie 2',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'stories' =>
                        array(
                            'available' => 1,
                            'collectionURI' => config('app.api_url') . '/characters/3/stories',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/stories/2',
                                    'name' => 'Story 2',
                                ),
                            ),
                            'returned' => 1,
                        ),
                        'events' =>
                        array(
                            'available' => 2,
                            'collectionURI' => config('app.api_url') . '/characters/3/events',
                            'items' =>
                            array(
                                0 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/2',
                                    'name' => 'Event 2',
                                ),
                                1 =>
                                array(
                                    'resourceURI' => config('app.api_url') . '/events/3',
                                    'name' => 'Event 3',
                                ),
                            ),
                            'returned' => 2,
                        ),
                        'urls' =>
                        array(
                            0 =>
                            array(
                                'type' => 'type 3',
                                'url' => 'url3.com',
                            ),
                        ),
                    ),
                ),
            ),
        );
    }
}
