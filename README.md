# Configurando ambiente

1. Instale o [Docker](https://www.docker.com/products/docker-desktop) em sua máquina
2. Crie uma cópia do arquivo .env.example e nomeie a cópia para .env
3. No terminal, execute dentro da raiz do projeto o comando `docker-compose up --build` e aguarde a instalacao finalizar
4. Em outro terminal, conecte-se com a CLI do conteiner usando o comando `docker exec -it marvel-api bash -c "sudo -u devuser /bin/bash"` 
    * Para os comandos abaixo, utilize a CLI do container aberta com o comando acima
5. Instale as dependencias usando o Composer por meio da CLI usando o comando `composer install`
6. Execute as migrations usando o Artisan por meio da CLI usando o comando `php artisan migrate --seed`
    * Caso dê erro na execução, efetue as operações: 
        1. Dê permissão de escrita para a pasta "storage"
        2. Execute o comando `php artisan config:clear`
7. Execute o comando `php artisan key:generate` para gerar uma chave para a aplicação
8. Acesse pelo seu navegador usando o endereco localhost:80
    * Caso voce já tenha alguma coisa rodando na porta 80, voce pode alterar a porta alterando o docker-compose.yml OU apontar uma máscara de DNS e definir o endereco dela na linha 14 do Dockerfile

# Executando testes
1. A rotina de testes deve ser executada no mesmo banco de produção. Para isso, deve-se alterar as chaves DB_DATABASE do arquivo .env.testing de "marveltest" para "marvel" e   DB_HOST para "db". Portanto, após executar os testes, para reinserir os dados de produção, utilizar o comando `php artisan db:seed`

2. Usando a CLI do container, execute o comando `php artisan test`
    *Em caso de erro, execute o comando `php artisan config:clear`

	**Os testes podem ser conferidos também na aba de pipelines do bitbucket.
**Caso seja necessário recriar o banco, utilizar o comando `php artisan migrate:reset`, seguido do `php artisan migrate --seed`
