FROM php:7.4-apache

# 1. Instala os pacotes de desenvolvimento e limpa o cache do apt
RUN apt-get update && apt-get install -y \
    sudo \
    git \
    unzip \
    zip \
    libzip-dev \
    libpq-dev \
 && rm -rf /var/lib/apt/lists/*

# 2. Configuracoes do Apache e root do documento
# RUN echo "ServerName marvel-api.local" >> /etc/apache2/apache2.conf

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# 3. mod_rewrite é necessário para o funcionamento do Laravel
RUN a2enmod rewrite headers

# 4. Adiciona extensoes do PHP
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN docker-php-ext-install \
    pdo \
    pdo_pgsql \
    pgsql \
    zip

# 5. Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# 6. Precisamos de um usuario com o mesmo UID/GID do host
# Para que, quando executados comandos pela CLI, todas as permissoes fiquem iguais
ARG uid
RUN useradd -G www-data,root -u $uid -d /home/devuser devuser
RUN mkdir -p /home/devuser/.composer && \
    chown -R devuser:devuser /home/devuser