<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComicTextObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comic_text_objects', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('type')->comment('The canonical type of the text object (e.g. solicit text, preview text, etc.).');
            $table->string('language')->comment('The IETF language tag denoting the language the text object is written in.');
            $table->text('text')->comment('The text.');
            
            $table->integer('comic_id');
            $table->foreign('comic_id')->references('id')->on('comics')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comic_text_objects');
    }
}
