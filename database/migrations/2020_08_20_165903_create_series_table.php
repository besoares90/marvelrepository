<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->comment('The canonical title of the series.');
            $table->text('description')->nullable()->comment('A description of the series.');
            
            $table->smallInteger('start_year')->nullable()->comment('The first year of publication for the series.');
            $table->smallInteger('end_year')->nullable()->comment('The last year of publication for the series (conventionally, 2099 for ongoing series) .');
            
            $table->string('rating')->nullable()->comment('The age-appropriateness rating for the series.');
            $table->string('type')->nullable();
            $table->text('thumbnail')->nullable();
            
            $table->integer('next_id')->nullable()->comment('A summary representation of the series which follows this series.');
            $table->integer('previous_id')->nullable()->comment('A summary representation of the series which preceded this series.');
            
            $table->foreign('next_id')->references('id')->on('series')->onDelete('cascade');
            $table->foreign('previous_id')->references('id')->on('series')->onDelete('cascade');
            
            //$table->jsonb('urls')->nullable()->comment('A set of public web site URLs for the resource.');

            //$table->timestamps();
            $table->timestampTz('modified_at');
            $table->timestampTz('created_at_series')->default('now()');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series');
    }
}
