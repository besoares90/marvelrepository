<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creators', function (Blueprint $table) {
            $table->increments('id');

            $table->string('first_name')->nullable()->comment('The first name of the creator');
            $table->string('middle_name')->nullable()->comment('The middle name of the creator');
            $table->string('last_name')->nullable()->comment('The last name of the creator.');
            $table->string('suffix')->nullable()->comment('The suffix or honorific for the creator');

            $table->text('thumbnail')->nullable()->comment('The representative image for this creator');
            
            //$table->jsonb('urls')->nullable()->comment('A set of public web site URLs for the resource');
      
            $table->timestampTz('modified_at');
            $table->timestampTz('created_at_creators')->default('now()');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creators');
    }
}
