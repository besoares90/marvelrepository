<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable()->comment('The title of the event.');
            $table->text('description')->nullable()->comment('A description of the event.');
            $table->dateTime('start', 0)->nullable()->comment('The date of publication of the first issue in this event.');
            $table->dateTime('end', 0)->nullable()->comment('The date of publication of the last issue in this event.');
            
            $table->text('thumbnail')->nullable()->comment('The representative image for this event.');
            
            //$table->jsonb('urls')->nullable()->comment('A set of public web site URLs for the event.');
            
            $table->integer('next_id')->nullable()->comment('A summary representation of the event which follows this event.');
            $table->integer('previous_id')->nullable()->comment('A summary representation of the event which preceded this event.');

            $table->foreign('next_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('previous_id')->references('id')->on('events')->onDelete('cascade');
            
            $table->timestampTz('modified_at');
            $table->timestampTz('created_at_events')->default('now()');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
