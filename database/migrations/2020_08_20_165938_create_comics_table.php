<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comics', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('digital_id')->nullable()->comment('The ID of the digital comic representation of this comic. Will be 0 if the comic is not available digitally.');
            $table->string('title')->nullable()->comment('The canonical title of the comic');
            $table->double('issue_number')->nullable()->comment('The number of the issue in the series (will generally be 0 for collection formats)');
            $table->text('variant_description')->default('')->nullable()->comment('If the issue is a variant (e.g. an alternate cover, second printing, or director’s cut), a text description of the variant.');
            $table->text('description')->nullable()->comment('The preferred description of the comic.');
            $table->string('isbn', 17)->nullable()->comment('The ISBN for the comic (generally only populated for collection formats). 13 digits plus 4 dashs (-)');
            $table->string('upc', 16)->nullable()->comment('The UPC barcode number for the comic (generally only populated for periodical formats).');
            $table->string('diamond_code', 15)->nullable()->comment('The Diamond code for the comic');
            $table->string('ean', 13)->nullable()->comment('The EAN barcode for the comic.');
            $table->string('issn', 9)->default('')->nullable()->comment('The ISSN barcode for the comic. 8 digits plus the dash (-)');
            $table->string('format', 50)->nullable()->comment('The publication format of the comic e.g. comic, hardcover, trade paperback');
            $table->integer('page_count')->nullable()->comment('The number of story pages in the comic');                        
            $table->text('thumbnail')->nullable()->comment('The representative image for this comic');

            $table->integer('serie_id')->nullable()->comment('A summary representation of the series to which this comic belongs.');
            $table->foreign('serie_id')->references('id')->on('series');
            
            //Also possible uses of JSON columns in case the database supports
            //$table->jsonb('urls')->nullable()->comment('A set of public web site URLs for the resource.');
            //$table->jsonb('text_objects')->nullable()->comment('A set of descriptive text blurbs for the comic');
            //$table->jsonb('prices')->nullable()->comment('A list of prices for this comic.');
            //$table->jsonb('images')->nullable()->comment('A list of promotional images associated with this comic');

            $table->timestampTz('modified_at');
            $table->timestampTz('created_at')->default('now()');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comics');
    }
}
