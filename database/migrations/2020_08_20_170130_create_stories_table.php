<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stories', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable()->comment('The story title.');
            $table->text('description')->nullable()->comment('A short description of the story.');
            $table->string('type')->nullable()->comment('The story type e.g. interior story, cover, text story.');
            $table->text('thumbnail')->nullable()->comment('The representative image for this story.');

            $table->integer('original_issue_id')->nullable()->comment('A summary representation of the issue in which this story was originally published.');
            $table->foreign('original_issue_id')->references('id')->on('comics')->onDelete('cascade');

            $table->timestampTz('modified_at');
            $table->timestampTz('created_at_stories')->default('now()');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stories');
    }
}
