<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComicPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comic_prices', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('type')->comment('A description of the price (e.g. print price, digital price)');
            $table->float('price')->default(0)->comment('The price (all prices in USD).');

            $table->integer('comic_id');
            $table->foreign('comic_id')->references('id')->on('comics')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comic_prices');
    }
}
