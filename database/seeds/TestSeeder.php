<?php

use App\Enums\Enums\ComicFormatEnum;
use App\Models\Character;
use App\Models\Comic;
use App\Models\ComicDate;
use App\Models\ComicImage;
use App\Models\ComicPrice;
use App\Models\ComicTextObject;
use App\Models\Creator;
use App\Models\Event;
use App\Models\Serie;
use App\Models\Story;
use App\Models\Url;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $serie1 = factory(Serie::class)->create([
                'title' => 'Serie 1',
                'description' => 'Description 1',
                'start_year' => 2010,
                'end_year' => 2020,
                'rating' => 'Rated T',
                'type' => 'collection',
                'modified_at' => '2010-01-01',
                'thumbnail' => 'thumbnailtest.com'
            ]);

            $serie2 = factory(Serie::class)->create([
                'title' => 'Serie 2',
                'description' => 'Description 2',
                'start_year' => 2011,
                'end_year' => 2021,
                'rating' => 'Rated T+',
                'type' => 'ongoing',
                'modified_at' => '2010-01-02',
                'thumbnail' => 'thumbnailtest2.com'
            ]);

            $story1 = factory(Story::class)->create([
                'title' => 'Story 1',
                'description' => 'Description 1',
                'type' => 'cover',
                'modified_at' => '2010-01-01',
                'thumbnail' => 'thumbnailtest1.com'
            ]);

            $story2 = factory(Story::class)->create([
                'title' => 'Story 2',
                'description' => 'Description 2',
                'type' => 'story',
                'modified_at' => '2010-01-02',
                'thumbnail' => 'thumbnailtest2.com'
            ]);

            $creator1 = factory(Creator::class)->create([
                'first_name' => 'Creator 1',
                'middle_name' => 'Middlename 1',
                'last_name' => 'Lastname 1',
                'suffix' => 'suffix 1',
                'modified_at' => '2010-01-01',
                'thumbnail' => 'thumbnailtest1.com'
            ]);

            $creator2 = factory(Creator::class)->create([
                'first_name' => 'Creator 2',
                'middle_name' => 'Middlename 2',
                'last_name' => 'Lastname 2',
                'suffix' => 'suffix 2',
                'modified_at' => '2010-01-02',
                'thumbnail' => 'thumbnailtest2.com'
            ]);

            $creator3 = factory(Creator::class)->create([
                'first_name' => 'Creator 3',
                'middle_name' => 'Middlename 3',
                'last_name' => 'Lastname 3',
                'suffix' => 'suffix 3',
                'modified_at' => '2010-01-03',
                'thumbnail' => 'thumbnailtest3.com'
            ]);

            $event1 = factory(Event::class)->create([
                'title' => 'Event 1',
                'description' => 'Description 1',
                'start' => '2010-01-01',
                'end' => '2015-01-01',
                'thumbnail' => 'thumbnailtest1.com',
            ]);

            $event2 = factory(Event::class)->create([
                'title' => 'Event 2',
                'description' => 'Description 2',
                'start' => '2012-01-01',
                'end' => '2015-01-01',
                'thumbnail' => 'thumbnailtest1.com',
                'previous_id' => $event1->id
            ]);

            $event3 = factory(Event::class)->create([
                'title' => 'Event 3',
                'description' => 'Description 3',
                'start' => '2013-05-04',
                'end' => '2018-01-06',
                'modified_at' => '2010-01-03',
                'thumbnail' => 'thumbnailtest1.com',
                'previous_id' => $event2->id
            ]);
            $event1->next_id = $event2->id;
            $event2->next_id = $event3->id;
            $event1->save();
            $event2->save();

            $character1 = factory(Character::class)->create([
                'name' => 'Character 1',
                'description' => 'Description 1',
                'thumbnail' => 'thumbnailtest1.com',
                'modified_at' => '2010-01-01',
            ]);

            $character1->urls()->createMany([
                [
                    'type' => 'type 1',
                    'url' => 'url1.com'
                ]
            ]);

            $character2 = factory(Character::class)->create([
                'name' => 'Character 2',
                'description' => 'Description 2',
                'thumbnail' => 'thumbnailtest2.com',
                'modified_at' => '2010-01-02',
            ]);

            $character2->urls()->createMany([
                [
                    'type' => 'type 1',
                    'url' => 'url1.com'
                ]
            ]);

            $character3 = factory(Character::class)->create([
                'name' => 'Character 3',
                'description' => 'Description 3',
                'thumbnail' => 'thumbnailtest3.com',
                'modified_at' => '2010-01-03',
            ]);
            
            $character3->urls()->createMany([
                [
                    'type' => 'type 3',
                    'url' => 'url3.com'
                ]
            ]);

            $comic1 = factory(Comic::class)->create([
                'title' => 'Comic 1',
                'digital_id' => 0,
                'issue_number' => 1,
                'variant_description' => '',
                'description' => 'Description 1',
                'isbn' => 'isbn1',
                'upc' => 'upc1',
                'diamond_code' => 'diamond1',
                'ean' => 'ean1',
                'issn' => 'issn1',
                'format' => ComicFormatEnum::COMIC,
                'page_count' => 1,
                'thumbnail' => 'thumb1.com',
            ]);
            $comic1->serie()->associate($serie1)->save();
            $comic1->stories()->syncWithoutDetaching([$story1->id]);
            $comic1->creators()->syncWithoutDetaching([$creator1->id, $creator2->id]);
            $comic1->characters()->syncWithoutDetaching([$character1->id, $character2->id]);
            $comic1->urls()->createMany([
                [
                    'type' => 'type 2',
                    'url' => 'url2.com'
                ]
            ]);
            $comic1->dates()->createMany([
                [
                    'type' => 'onsaleDate',
                    'date' => '2011-01-01',
                    'comic_id' => $comic1->id
                ],
                [
                    'type' => 'focDate',
                    'date' => '2011-01-02',
                    'comic_id' => $comic1->id
                ]
            ]);
            $comic1->prices()->createMany([
                [
                    'type' => 'printPrice',
                    'price' => 2.99,
                    'comic_id' => $comic1->id
                ]
            ]);
            $comic1->images()->createMany([
                [
                    'path' => 'path1.com',
                    'comic_id' => $comic1->id
                ]
            ]);
            $comic1->text_objects()->createMany([
                [
                    'type' => 'solicit text',
                    'language' => 'de',
                    'text' => 'text 1',
                    'comic_id' => $comic1->id
                ]
            ]);
            $comic1->events()->saveMany([$event1, $event2]);


            //--
            $comic2 = factory(Comic::class)->create([
                'title' => 'Comic 2',
                'digital_id' => 1,
                'issue_number' => 2,
                'variant_description' => 'Variant Description 1',
                'description' => 'Description 2',
                'isbn' => 'isbn2',
                'upc' => 'upc2',
                'diamond_code' => 'diamond2',
                'ean' => 'ean2',
                'issn' => 'issn2',
                'format' => ComicFormatEnum::HARDCOVER,
                'page_count' => 2,
                'thumbnail' => 'thumb2.com',
            ]);
            $comic2->serie()->associate($serie2)->save();
            $comic2->stories()->syncWithoutDetaching([$story2->id]);
            $comic2->creators()->syncWithoutDetaching([$creator2->id, $creator3->id]);
            $comic2->characters()->syncWithoutDetaching([$character2->id, $character3->id]);
            $comic2->urls()->createMany([
                [
                    'type' => 'type 2',
                    'url' => 'url2.com'
                ]
            ]);
            $comic2->dates()->createMany([
                [
                    'type' => 'onsaleDate',
                    'date' => '2017-01-01',
                    'comic_id' => $comic2->id
                ],
                [
                    'type' => 'focDate',
                    'date' => '2018-01-02',
                    'comic_id' => $comic2->id
                ]
            ]);
            $comic2->prices()->createMany([
                [
                    'type' => 'printPrice',
                    'price' => 5,
                    'comic_id' => $comic2->id
                ]
            ]);
            $comic2->images()->createMany([
                [
                    'path' => 'path2.com',
                    'comic_id' => $comic2->id
                ]
            ]);
            $comic2->text_objects()->createMany([
                [
                    'type' => 'shorter text',
                    'language' => 'en-us',
                    'text' => 'text 2',
                    'comic_id' => $comic2->id
                ]
            ]);
            $comic2->events()->saveMany([$event2, $event3]);
            $comic2->variants()->syncWithoutDetaching($comic1);
            $comic2->collections()->syncWithoutDetaching($comic1);
            $comic2->collected_issues()->syncWithoutDetaching($comic1);
        });
    }
}
