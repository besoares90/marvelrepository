<?php

use App\Models\Character;
use App\Models\Comic;
use App\Models\ComicDate;
use App\Models\ComicImage;
use App\Models\ComicPrice;
use App\Models\ComicTextObject;
use App\Models\Creator;
use App\Models\Event;
use App\Models\Serie;
use App\Models\Story;
use App\Models\Url;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $previousEvent = null;

            $events = factory(Event::class, 75)->create()->each(function ($e) {
                $e->urls()->saveMany(factory(Url::class, rand(1, 3))->create());
            });

            for ($i = 0; $i < 70; ++$i) {
                if (rand(1, 10) > 6) {
                    $events[$i+1]->previous_id = $events[$i]->id;
                    $events[$i+1]->next_id = $events[$i+2]->id;
                    $events[$i+1]->save();
                }
            }

            $creators = factory(Creator::class, 40)->create()->each(function ($c) {
                $c->urls()->saveMany(factory(Url::class, rand(1, 3))->create());
            });

            $stories = factory(Story::class, 300)->create();

            $characters = factory(Character::class, 80)->create()->each(function ($c) {
                $c->urls()->saveMany(factory(Url::class, rand(1, 3))->create());
            });

            $series = factory(Serie::class, 200)->create()->each(function ($c) {
                $c->urls()->saveMany(factory(Url::class, rand(1, 3))->create());
            });

            $comics = factory(Comic::class, 150)->create()->each(function ($c) use ($series, $stories, $creators, $characters, $events) {
                $c->urls()->saveMany(factory(Url::class, rand(1, 3))->create());
                $c->dates()->saveMany(factory(ComicDate::class, rand(1, 3))->create(['comic_id' => $c->id]));
                $c->prices()->saveMany(factory(ComicPrice::class, rand(1, 3))->create(['comic_id' => $c->id]));
                $c->images()->saveMany(factory(ComicImage::class, rand(1, 3))->create(['comic_id' => $c->id]));
                $c->text_objects()->saveMany(factory(ComicTextObject::class, rand(1, 3))->create(['comic_id' => $c->id]));

                if (rand(1, 10) > 6) {
                    $c->serie()->associate($series[rand(0, sizeof($series) - 1)])->save();
                }

                $numberOfStories = rand(1, 3);

                for ($i = 0; $i < $numberOfStories; ++$i) {
                    $c->stories()->syncWithoutDetaching($stories[rand(0, sizeof($series) - 1)]);
                }

                $numberOfCreators = rand(1, 3);
                for ($i = 0; $i < $numberOfCreators; ++$i) {
                    $c->creators()->syncWithoutDetaching($creators[rand(0, sizeof($creators) - 1)]);
                }

                $numberOfCharacters = rand(1, 4);
                for ($i = 0; $i < $numberOfCharacters; ++$i) {
                    $c->characters()->syncWithoutDetaching($characters[rand(0, sizeof($characters) - 1)]);
                }

                $numberOfEvents = rand(1, 2);
                for ($i = 0; $i < $numberOfEvents; ++$i) {
                    $c->events()->syncWithoutDetaching($events[rand(0, sizeof($events) - 1)]);
                }                
            });

            // for ($i = 0; $i < 120; ++$i){
            //     $randomComicNumber = rand(1, sizeof($comics) - 1);

            //     if (rand(1, 10) > 6) {

            //     }
            // }

        });
    }
}
