<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Creator;
use Faker\Generator as Faker;

$factory->define(Creator::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'middle_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'suffix' => $faker->title,
        'thumbnail' => $faker->imageUrl(640, 480),
    ];
});
