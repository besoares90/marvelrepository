<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ComicDate;
use Faker\Generator as Faker;

$factory->define(ComicDate::class, function (Faker $faker) {
    return [
        'type' => $faker->word,
        'date' => $faker->dateTime('now'),
    ];
});
