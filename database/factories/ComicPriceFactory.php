<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ComicPrice;
use Faker\Generator as Faker;

$factory->define(ComicPrice::class, function (Faker $faker) {
    return [
        'type' => $faker->word,
        'price' => $faker->randomNumber(2),
    ];
});
