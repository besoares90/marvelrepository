<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ComicTextObject;
use Faker\Generator as Faker;

$factory->define(ComicTextObject::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement($array = array ('solicit text', 'preview text', 'shorter text', 'custom text')),
        'language' => $faker->randomElement($array = array ('en-us', 'en-gb', 'fr', 'pt-br', 'de')),
        'text' => $faker->text(200),
    ];
});
