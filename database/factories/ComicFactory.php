<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Enums\Enums\ComicFormatEnum;
use App\Models\Comic;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Comic::class, function (Faker $faker) {
    return [
        'digital_id' => $faker->randomNumber(),
        'title' => $faker->sentence,
        'issue_number' => $faker->randomNumber(),
        'variant_description' => '',
        'description' => $faker->text(200),
        'isbn' => $faker->isbn13,
        'upc' => $faker->isbn13,
        'diamond_code' => Str::random(10),
        'ean' => $faker->ean13,
        'issn' => $faker->ean8,
        'format' => $faker->randomElement($array = ComicFormatEnum::getValues()),
        'page_count' => $faker->randomNumber(2),
        'thumbnail' => $faker->imageUrl(640, 480),
    ];
})->state(Comic::class, 'variant', function ($faker) {
    return ['variant_description' => $faker->text(200)];
});
