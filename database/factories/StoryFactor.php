<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Story;
use Faker\Generator as Faker;

$factory->define(Story::class, function (Faker $faker) {

    return [
       'title' => $faker->sentence,
       'description' => $faker->text(200),       
       'type' => $faker->randomElement($array = array('story', 'cover', 'text story')),       
       'thumbnail' => $faker->imageUrl(640, 480),
    ];

});
