<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Serie;
use Faker\Generator as Faker;

$factory->define(Serie::class, function (Faker $faker) {
    $year1 = intval($faker->year());
    $year2 = intval($faker->year());

    return [
        'title' => $faker->sentence,
        'description' => $faker->text(200),
        'start_year' => $year1 > $year2 ? $year2 : $year1,
        'end_year' => $year1 > $year2 ? $year1 : $year2,
        'type' => $faker->randomElement($array = array('collection', 'one shot', 'limited', 'ongoing')),
        'rating' => $faker->randomElement($array = array('Rated T', 'Rated T+', 'MARVEL PSR', 'T', 'T+')),

        'thumbnail' => $faker->imageUrl(640, 480),
    ];
});
