<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ComicImage;
use Faker\Generator as Faker;

$factory->define(ComicImage::class, function (Faker $faker) {
    return [
        'path' => $faker->imageUrl(640, 480),
    ];
});
