<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Url;
use Faker\Generator as Faker;

$factory->define(Url::class, function (Faker $faker) {
    return [
        'type' => $faker->word,
        'url' => $faker->url,
    ];
});
