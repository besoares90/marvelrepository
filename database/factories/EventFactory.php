<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
       'title' => $faker->sentence,
       'description' => $faker->text(200),
       'start' => $faker->dateTime(),
       'end' => $faker->dateTime(),
       'thumbnail' => $faker->imageUrl(640, 480),
    ];
});
