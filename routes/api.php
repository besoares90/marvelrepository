<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('v1/public')->group(function () {
    Route::get('/characters', 'CharacterController@get');
    Route::get('/characters/{id}', 'CharacterController@getById')->where('id', '[0-9]+');
    Route::get('/characters/{id}/comics', 'CharacterController@getCharacterComics')->where('id', '[0-9]+');
    Route::get('/characters/{id}/events', 'CharacterController@getCharacterEvents')->where('id', '[0-9]+');
    Route::get('/characters/{id}/series', 'CharacterController@getCharacterSeries')->where('id', '[0-9]+');
    Route::get('/characters/{id}/stories', 'CharacterController@getCharacterStories')->where('id', '[0-9]+');
});
